<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


        Route::group(['prefix' => 'admin'], function () {
            Voyager::routes();
        });
      /**
         * Pages route 
         */
    // Home routes...
    Route::get('/home', 'PagesController@getHome'); 
    Route::get('/', 'PagesController@getHome'); 

    // about routes...
    Route::get('/about', 'PagesController@getAbout'); 
    // communautes routes..
    Route::get('/communautes', 'PagesController@getCommunautes'); 
    //  contact routes..
    Route::get('/contact', 'PagesController@getContact'); 
    //  contact routes..
    Route::get('/games', 'PagesController@getjeux'); 
    //  contact routes..
    Route::get('/news', 'PagesController@getnews'); 




    /**
    * session routes
    */
    Auth::routes();

    Route::get('/dashboard', 'PagesController@getDashboard',['middleware' => 'auth', function()
    {
        //
        Route::get('/dashboard', 'PagesController@getDashboard'); 

    }]);
    
    // post dashboard routes...
    // Route::get('/dashboard', 'PagesController@getDashboard'); 
    // Route::resource('/dashboard/posts','PostsController');
    Route::get('/dashboard/membres', 'PagesController@getDashboardmembres'); 
    Route::get('/dashboard/posts', 'PagesController@getDashboardpost'); 

    Route::get('/dashboard/posts', 'PostsController@create'); 

    
    // profile routes...
    Route::resource('profile','UserController');
    // Route::get('/profile/','UserController@index');
    
    Route::get('profile/{id}/edit/','UserController@edit');

    // post routes...
    Route::resource('post','PostsController');
    Route::get('/post/{$id}-{$title}/edit','PostsController@edit');
    

Route::group(['middleware' => ['web']], function (){
    
  

        // Route::group(array('before' => 'auth'), function()
    // {
        // post routes...
        // Route::resource('profile','UserController');
        // Route::get('profile/edit','UserController@edit');
        // post routes...
        // Route::resource('post','PostsController');
        // Route::get('/post/{$id}/edit','PostsController@edit');
    // });
});





