<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


        Route::group(['prefix' => 'admin'], function () {
            Voyager::routes();
        });
      /**
         * Pages route 
         */
    // Home routes...
    Route::get('/home', 'PagesController@getHome')->name('home'); 
    Route::get('/', 'PagesController@getHome')->name('home'); 

    // about routes...
    Route::get('/about', 'PagesController@getAbout'); 
    // communautes routes..
    Route::get('/communautes', 'PagesController@getCommunautes'); 
    //  contact routes..
    Route::get('/contact', 'PagesController@getContact'); 
    //  contact routes..
    Route::get('/games', 'PagesController@getjeux'); 
    //  contact routes..
    Route::get('/news', 'PagesController@getnews'); 
    Route::get('/search', 'PagesController@getSearch'); 



    /**
    * session routes
    */
    Auth::routes();

    // Route::get('/dashboard', 'PagesController@getDashboard',['middleware' => 'auth', function()
    // {
    //     //
    //     Route::get('/dashboard', 'PagesController@getDashboard'); 

    // }]);
    Route::get('/dashboard', 'PagesController@getDashboard')->name('dashboard'); 

    // Route::get('/dashboard', 'PagesController@getDashboard'); 
    // Route::resource('/dashboard/posts','PostsController');
    
    Route::get('/dashboard/member', 'PagesController@getDashboardmembres')->name('dashboard.member'); 
    // post dashboard routes...

    Route::get('/dashboard/posts', 'PagesController@getDashboardpost')->name('dashboard.posts'); 

    Route::get('/dashboard/posts/create', 'PostsController@create')->name('dashboard.posts.create'); 
    
    // profile routes...
        //  Route::resource('/dashboard/account/','UserController');
        //  Route::get('/dashboard/account/{id}/edit/','UserController@edit');

        Route::resource('dashboard/account','UserController')->name('dashboard');
        Route::get('dashboard/account/edit/','UserController@edit');

    // post routes...
    Route::resource('post','PostsController');
    
    // Route::get('/post/{$id}-{$title}/edit','PostsController@edit');
    Route::get('/poste/{slug}-{id}/', 'PostsController@show');
    // Route::resource('poste','PostsController');

Route::group(['middleware' => ['web']], function (){
    
  

        // Route::group(array('before' => 'auth'), function()
    // {
        // post routes...
        // Route::resource('profile','UserController');
        // Route::get('profile/edit','UserController@edit');
        // post routes...
        // Route::resource('post','PostsController');
        // Route::get('/post/{$id}/edit','PostsController@edit');
    // });
});





