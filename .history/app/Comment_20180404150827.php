<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use User;
Use post;

class Comment extends Model
{
    //
    public function post()
    {
        return $this->belongsTo(Post::class);
    }
}
