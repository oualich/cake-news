<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use User;
Use Post;

class Comment extends Model
{
    //
    use Translatable,
    HasRelationships;
    public function post()
    {
        return $this->belongsTo(Post::class);
    }
}
