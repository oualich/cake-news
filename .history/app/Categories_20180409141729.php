<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categories extends Model
{
    //

    public function category()
    {
        return $this->belongsTo(Post::class);
    }
}
