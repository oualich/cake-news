<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\User;
use View;

class PostsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $posts = Post::orderBy('created_at', 'desc')->paginate(5);
        return view('posts.index')->with('posts', $posts);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('posts.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, [
            'title' => 'required',
            'image' => 'required',
            'body' => 'required',
        ]);

        // create todo
        $posts = new Post;

        $posts->title = $request->input('title');
        $posts->body = $request->input('body');
        $posts->excerpt = $request->input('excerpt');
        $posts->slug = $request->input('slug');
        $posts->author_id = $request->input('author_id');
        $posts->category_id = $request->input('category_id');
        $posts->status = $request->input('status');

        $file = $request->file('image');
        $fileName = $file->getClientOriginalName();
        $destinationPath = base_path() . '/storage/app/public/posts/';
        $move = $request->file('image')->move($destinationPath, $fileName);
        $namefile = 'posts/' .$fileName;
        $posts->image =$namefile;
        // $request->file('image')->move(public_path('image'), $request->file('image')->getClientOriginalName());
        // $data = $request->except(['image']);
        // $posts->image = $data['image'] = public_path('image') . '/' . $request->file('image')->getClientOriginalName();
        


        $posts->save();

        return redirect('/')->with('success', 'Post successfully created!'); 

    }
 
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $posts = Post::find($id);
        
        return view('posts.show')->with('posts', $posts);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @param  int  $slug
     * @return \Illuminate\Http\Response
     */
    public function edit($slug, $id)
    {
        //
        $posts = Post::find($slug, $id);
        return view('posts.edit')->with('posts', $posts);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $posts = Post::find($id);

        $posts->title = $request->input('title');
        $posts->body = $request->input('body');

        $posts->excerpt = $request->input('description');
        
       

        $posts->save();

        return redirect('/')->with('success', 'Post successfully update!'); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $posts = Post::find($id);
        $posts->delete();
        return redirect('/')->with('success','Post successfully delete!');
    }
}
