<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;

use App\User;


class UserController extends Controller
{
    public function index()
    {
        //
        $posts = User::all();
        return view('users.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('users.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, [
            'text' => 'required'
        ]);

        // create todo
        $posts = new User;
        $posts->text = $request->input('text');
        $posts->body = $request->input('body');
        $posts->due = $request->input('due');

        $posts->save();

        return redirect('/')->with('success', 'Todo create'); 

    }
 
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $posts = User::find($id);
        return view('users.show')->with('posts', $posts);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $posts = User::find($id);
        return view('users.edit')->with('posts', $posts);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // 
        // $id = User::user()->id;
        // $posts = User::user()->$id;

        // $posts->name = $request->input('name');
        // $posts->firstname = $request->input('firstname');
        // $posts->email = $request->input('email');
        // $posts->bio = $request->input('bio');

        // $posts->update($request->all());

        $user = User::find($id);
    
        $user->firstname = $request->input('firstname');
        $user->email = $request->input('email');
        $user->name = $request->input('name');
        $user->bio = $request->input('bio');

        $user->save();
        // $user = User::find($id);
        // $user->update([
        //     'name' => $request->name,
        //     'email' => $request->email,
        //     'bio' => $request->bio
        // ]);

        return redirect('/')->with('success', 'user update'); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $posts = Posts::find($id);
        $posts->delete();
        return redirect('/')->with('succes','Todo delete');
    }
}
