<?php

namespace App\Http\Controllers;
// use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Input;
use App\Post;

use App\User;

use View;

use Request;


class PagesController extends Controller
{
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function getHome(){
        // $posts = Post::all()
        // ->where('status' , 'PUBLISHED');
                // $request = Post::where('status' , 'PUBLISHED')->paginate(6);
            $request = Post::paginate(5);
        
                
        $posts = Post::where('status' , 'PUBLISHED')->paginate(8);

        // $posts = Post::get();
        // $posts = Post::withCount('posts')->get();
        // return view('home')->with(array('posts'=>$posts,'posts'=>$sidebar));
        return view('home',['posts'=>$posts,'request'=>$request]);

        // return view('home', compact('posts'))->with('posts', $posts);

    
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function getjeux(){

        // $posts = Post::paginate(8);;
        $posts = Post::search()
        ->orderBy('title')
        ->where('category_id' , '2')
        ->paginate(8);

        // $posts = Post::get();
        // $posts = Post::withCount('posts')->get();

        return view('pages.jeux', compact('posts'))->with('posts', $posts);

    
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
     public function getnews(){

        // $posts = Post::paginate(8);;
        $posts = Post::search()
        ->orderBy('title')
        ->where('category_id' , '1')
        ->paginate(8);
                // $posts = Post::get();
        // $posts = Post::withCount('posts')->get();

        return view('pages.news', compact('posts'))->with('posts', $posts);

    
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
     public function getAbout(){
    
        return view('pages.about');
    
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
     public function getContact(){
    
        return view('pages.contact');
    
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     public function getDashboard(){
    
        $posts = Post::all();
        return view('pages.dashboard')->with('posts', $posts);
    
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     public function getCommunautes(){
    
        $users = User::all();
        return view('pages.communautes')->with('users', $users);
    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function getDashboardpost()
    {
        //
        $posts = Post::orderBy('created_at', 'desc')->paginate(5);
        return view('admin.posts')->with('posts', $posts);
    }

        /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function getDashboardmembres()
    {
        //
        $users = User::orderBy('created_at', 'desc')->paginate(5);
        return view('admin.membres')->with('users', $users);

        // $users = User::orderBy('created_at', 'desc')->paginate(5);
        // return view('admin.membres')->with('users', $users);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
     public function destroy($id)
    {
        //
        $posts = Post::find($id);
        $posts->delete();
        return redirect('/')->with('succes','posts delete');
    }

}

