<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {        
        
        // return view($posts = Posts::latest()->paginate(5));
        // return view('home');
        
    }
    public function getSidebare(){
        // $posts = Post::all()
        // ->where('status' , 'PUBLISHED')
        // ;
        
        $sidebar = Post::where('status' , 'PUBLISHED')->paginate(8);

        // $posts = Post::get();
        // $posts = Post::withCount('posts')->get();

        return view('inc.sidebare', compact('posts'))->with('posts', $sidebar);

    
    }
}
