<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use User;
Use comment;

class Post extends Model
{
    public function image()
        {
            return [
            'image'       => 'required|mimes:png'
            ];
        }
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */

    public function category()
        {
            return $this->belongsTo(Post::modelClass('Category'));
        }

    
        /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    
    public function scopeSearch($q)
        {
            return empty(request()->search) ? $q : $q->where('title', 'LIKE', '%'.request()->search.'%')->orWhere('excerpt', 'LIKE', '%'.request()->search.'%');
        }


        /**
         * Get the author of the post.
         */
    public function user()
        {
            return $this->belongsTo('User\class')->select('name');
            // return $this->belongsTo('App\User')->withDefault(function ($user) {
            // $user->name = 'Guest Author';    
                        
            // });
        }

    public function author()
    {
        return $this->belongsTo(User::class);
    }    
    }
