<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use User;
Use Post;


class Categories extends \TCG\Voyager\Models\Category
{
    //
    protected $table = 'Categories';

    protected $fillable = [
    'name',
    ];
    public function category()
    {
         return $this->belongsTo(Post::class);
        // return $this->belongsTo(Voyager::modelClass('Categories'));

    }
}
