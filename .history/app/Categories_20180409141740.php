<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
Use Post;
class Categories extends Model
{
    //

    public function category()
    {
        return $this->belongsTo(Post::class);
    }
}
