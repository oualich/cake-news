<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $exception
     * @return void
     */
    private $sentryID;

    public function report(Exception $e)
    {
        if ($this->shouldReport($e)) {
            // bind the event ID for Feedback
            $this->sentryID = app('sentry')->captureException($e);
        }
        parent::report($e);
    }

    public function render($request, Exception $e)
    {
        return response()->view('errors.500', [
            'sentryID' => $this->sentryID,
        ], 500);
    }

    
}
