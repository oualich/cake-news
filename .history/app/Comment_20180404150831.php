<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use User;
Use Post;

class Comment extends Model
{
    //
    public function post()
    {
        return $this->belongsTo(Post::class);
    }
}
