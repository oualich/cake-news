<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Post extends Model
{
    public function image()
        {
            return [
            'image'       => 'required|mimes:png'
            ];
        }
    //
        /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function category()
    {
        return $this->belongsTo(Post::modelClass('Category'));
    }

        /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function uniques() {
        $uniques = array();
        foreach ($posts as $post) {
            $uniques[$post->code] = $post; // Get unique country by code.
        }
    
        dd($uniques);
    }
    
    public function scopeSearch($q)
        {
            return empty(request()->search) ? $q : $q->where('title', 'LIKE', '%'.request()->search.'%'),->orWhere('customer.phone', 'LIKE', "%$findcustomer%");
        }
}
