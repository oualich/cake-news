@extends('layouts.app')
@section('title')
    Home 
@endsection
@section('script')
<script defer src="https://use.fontawesome.com/releases/v5.0.8/js/all.js"></script>
@endsection

@section('content')
    <div class="container-fluid">        
        <div class="row">
            <div class="col-md-10 col-md-offset-2">
                <div class="">
                        <div class="panel-body">
                            @if (session('status'))
                                <div class="alert alert-success">
                                    {{ session('status') }}
                                </div>
                            @endif
                        </div>
                </div>
            </div>
<style>

.bord_rubrique {
    border-top-color: #1f0d67; 
    border-top: 3px solid;
    background-color:white;
}
 .entete_deroule {
    color: #1f0d67;
    
}

.entete_deroule {
    display: block;
    padding: 10px 0;
    margin: 0 0 16px;
    border-bottom: 1px solid #eef1f5;
    text-align: center;
        background-color:white;

}
.entete_deroule, #ariane_az a:hover, #ariane_az .obf:hover, .couleur_rubrique {
    color: #2e3942;
}
.tt5, .tt5_capital {
    font-size: 2rem;
    line-height: 120%;
    padding: 0 0 .3rem;
}
.box-home {
        padding-left: 5px;

}
</style>
            <div id="news">
                <div class="bord_rubrique">
                    <h1 class="entete_deroule tt5">News
                    </h1>
                </div>
                @if(count($posts) > 0)

                    @foreach ($posts->take(8) as $post)
                        <p> Posted by <strong> {{ $post->author}} </strong> on {!!  $posts->created_at->format('l jS \\of F Y h:i:s A') !!}</p>

                        {{-- condition pour afficher la categorie  --}}
                        @if ($post->category_id == '1' && $post->category_id = str_replace('1', 'Actualite', $post->category_id))
                        {{--  {{ $post->category_id = str_replace('1', ' actualite', $post->category_id) }}  --}}
                                                {{-- condition pour afficher un article publier  --}}                            
                                <div class="col-md-3 col-lg-3 box-home">
                                    <ul class="col" data-id="{{$post->id}}">                        
                                                    {{--  choix 1  --}}
                                            <li class="bcolor">
                                                <a href="post/{{ $post->id }}/">
                                                    <img src="@if( !filter_var($post->image, FILTER_VALIDATE_URL)){{ Voyager::image( $post->image ) }}@else{{ $post->image }}@endif"
                                                            class="img-responsive img-height"
                                                            alt="{{ $post->title }} avatar">
                                                        <h3>{!!$post->title!!}</h3>
                                                        <p class="">{!!$post->excerpt!!}</p>
                                                    </a>
                                                    {{--  <span class="label">{!!$post->body!!}</span>  --}}
                                                    <a href="#news" class="tag">{{ $post->category_id}} </a>
                                            </li>
                                    </ul>
                                </div>
                        @else
                        @endif
                    @endforeach  
                    <div class="col-md-8 col-offset-2">
                        <a class="btn btn-primary btn-sm text-center">  Read more news</a>       
                    </div>
                @else
                    <h1><strong>aucun article na encore etait publier</strong></h1>
                @endif
            </div>
        </div>
        <div class="row">
                    
            <div id="jeux">
                <div class="bord_rubrique">
                    <h1 class="entete_deroule tt5">Jeux
                    </h1>
                </div>
                @if(count($games) > 0)
                
                    @foreach ($games->take(8) as $post)
                    {{-- condition pour afficher la categorie  --}}
                        @if ($post->category_id == '2' && $post->category_id = str_replace('2', 'Jeux', $post->category_id)) 
                        
                        {{--  {{ $post->category_id = str_replace('1', ' actualite', $post->category_id) }}  --}}
                            
                            {{-- condition pour afficher un article publier  --}}
                        
                            
                                <div class="col-md-3 col-lg-3 box-home">
                                    <ul class="col" data-id="{{$post->id}}">                        
                                                    {{--  choix 1  --}}
                                            <li class="bcolor">
                                                <a href="post/{{ $post->id }}/">
                                                    <img src="@if( !filter_var($post->image, FILTER_VALIDATE_URL)){{ Voyager::image( $post->image ) }}@else{{ $post->image }}@endif"
                                                            class="img-responsive img-height"
                                                            alt="{{ $post->title }} avatar">
                                                        <h3>{!!$post->title!!}</h3>
                                                        <p class="">{!!$post->excerpt!!}</p>
                                                    </a>
                                                    {{--  <span class="label">{!!$post->body!!}</span>  --}}
                                                    <a href="#jeux" class="tag">{{ $post->category_id}} </a>
                                            </li>
                                    </ul>
                                </div>
                                        @else
                        @endif

                    @endforeach  
                    <div class="col-md-8 col-offset-2">
                        <a class="btn btn-primary btn-sm text-center"> Read more news</a>       
                    </div> 
                @else
                    <h1><strong>aucun article na encore etait publier</strong></h1>
                @endif
            </div>
        </div>
           
    </div>
    
@endsection
{{--  @auth
    // The user is authenticated...
@endauth

@guest
    // The user is not authenticated...
@endguest  --}}