@extends('layouts.app')
@if (Auth::user())

@section('script')

<script src="https://cloud.tinymce.com/stable/tinymce.min.js"></script>
<script>tinymce.init({ selector:'textarea' });</script>

@endsection
    @section('content')
    <div class="col-md-6 col-md-offset-3">
    {!! Form::open(['action' => ['UserController@update', Auth::user()->id], 'methode'=>'POST']) !!}
        <div class="panel-body">
            <div class="form-group">
                    <img src="@if( !filter_var(Auth::user()->avatar, FILTER_VALIDATE_URL)){{ Voyager::image( Auth::user()->avatar ) }}@else{{ Auth::user()->avatar }}@endif"
                    style="width:200px; height:auto; clear:both; display:block; padding:2px; border:1px solid #ddd; margin-bottom:10px;     margin: 0 auto;"
                        alt="{{ Auth::user()->avatar }} avatar"><br>
                {{ Form::file('image') }}
            </div>
        </div>
        <div class="form-group">
            {{ Form::bsText('name', Auth::user()->name) }}
        </div>
        {{ Form::bsText('firstname', Auth::user()->firstname) }}
        {{ Form::bsText('email', Auth::user()->email) }}
        {{ Form::bsTextArea('bio', Auth::user()->bio) }}
        {{ Form::hidden('_method', 'PUT') }}
        {{ Form::bsSubmit('Submit', ['class' => 'btn btn-primary'])}}
    {!! Form::close() !!}
    </div>
@endsection
@else
        <script>window.location.href = "/login";</script>   
@endif