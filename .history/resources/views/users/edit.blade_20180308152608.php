@extends('layouts.app')
@if (Auth::user())

@section('script')

<script src="https://cloud.tinymce.com/stable/tinymce.min.js"></script>
<script>tinymce.init({ selector:'textarea' });</script>

@endsection
    @section('content')
    <div class="col-md-3">
            
            <div style="position:relative; z-index:9; text-align:center;">
                    <img class="profile-img" src="./storage/{!!ucwords(Auth::user()->avatar)!!}"                    class="avatar"
                    style="border-radius:50%; width:150px; height:150px; border:5px solid #fff;"
                    alt="{{ Auth::user()->name }} avatar">
                <h4>{{ ucwords(Auth::user()->name) }}</h4>
                <p>{{ Auth::user()->firstname }}</p>
            <div class="user-email text-muted">{{ ucwords(Auth::user()->email) }}</div>
            <br>
            <p>{{ Auth::user()->bio }}</p>
            <a href="http://127.0.0.1:8000/user/{{Auth::user()->id }}/edit" class="btn btn-primary">{{ __('voyager.profile.edit') }}</a>
        <br>
    </div>
    <div class="col-md-6 ">
    {!! Form::open(['action' => ['UserController@update', Auth::user()->id], 'methode'=>'POST']) !!}
        <div class="panel"> 
           
                <div class="form-group">
                        <img src="@if( !filter_var(Auth::user()->avatar, FILTER_VALIDATE_URL)){{ Voyager::image( Auth::user()->avatar ) }}@else{{ Auth::user()->avatar }}@endif"
                        style="width:200px; height:auto; clear:both; display:block; padding:2px; border:1px solid #ddd; margin-bottom:10px;     margin: 0 auto;"
                            alt="{{ Auth::user()->avatar }} avatar"><br>
                    {{ Form::file('image') }}
                </div>
            </div>
            <div class="panel"> 
                <div class="form-group">
                    {{ Form::bsText('name', Auth::user()->name) }}
                </div>
            </div>
            <div class="panel"> 
                <div class="form-group">
                    {{ Form::bsText('firstname', Auth::user()->firstname) }}
                </div>
            </div>
            <div class="panel"> 
                <div class="form-group">
                    {{ Form::bsText('email', Auth::user()->email) }}
                </div>
            </div>
        
            <div class="panel">
                <div class="form-group">
                    {{ Form::bsTextArea('bio', Auth::user()->bio) }}
                </div>
            </div>
            <div class="panel">
                <div class="form-group">
                    {{ Form::hidden('_method', 'PUT') }}
                </div>
            </div>
            <div class="panel">
                <div class="form-group">
                    {{ Form::bsSubmit('Submit', ['class' => 'btn btn-primary'])}}
                </div>
            </div>
        </div>
    {!! Form::close() !!}
    </div>
@endsection
@else
        <script>window.location.href = "/login";</script>   
@endif