@extends('layouts.app')
@if (Auth::user())
    @section('content')
    <div class="col-md-12">
    {!! Form::open(['action' => ['UserController@update', Auth::user()->id], 'methode'=>'POST']) !!}
        <div class="panel-body">
                <img src="@if( !filter_var(Auth::user()->avatar, FILTER_VALIDATE_URL)){{ Voyager::image( Auth::user()->avatar ) }}@else{{ Auth::user()->avatar }}@endif"
                style="width:200px; height:auto; clear:both; display:block; padding:2px; border:1px solid #ddd; margin-bottom:10px;"
                    alt="{{ Auth::user()->avatar }} avatar">
            {{ Form::file('image') }}
        </div>
        {{ Form::bsText('name', Auth::user()->name) }}
        {{ Form::bsText('firstname', Auth::user()->firstname) }}
        {{ Form::bsText('email', Auth::user()->email) }}
        {{ Form::bsTextArea('bio', Auth::user()->bio) }}
        {{ Form::hidden('_method', 'PUT') }}
        {{ Form::bsSubmit('Submit', ['class' => 'btn btn-primary'])}}
    {!! Form::close() !!}
@endsection
@else
        <script>window.location.href = "/login";</script>   
@endif