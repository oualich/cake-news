@section('sidebar')
    <div class="well">
        <h6 class="text-center bg-title-h6">En continu</h6>
        {{--  @foreach ($posts as $post)  --}}
         @foreach ($posts->take(8) as $post)
        <ul>
            <li><a href="post/{{ $post->id }}/">{{$post->title}}</a></li>
        </ul>    
        @endforeach
    </div>
    <style>
    .panel-sidebar{
    margin-left: -28px;
    }
    </style>
        <div class="panel-heading panel-sidebar" style="display:flex; justify-content:center;align-items:center;">
            {{$posts->links()}}
        </div>  
