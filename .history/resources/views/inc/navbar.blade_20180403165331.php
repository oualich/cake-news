@section('navbar')
<header>
    <nav class="navbar navbar-default navbar-static-top">
        <div class="container-fluid">
            <div class="navbar-header">
                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse" aria-expanded="false">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Branding Image -->
                <a class="navbar-brand" href="{{ url('/') }}">
                    {{ config('app.name', 'Laravel') }}
                </a>
            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
                <ul class="nav navbar-nav">

                    @if (Request::is('dashboard') || Request::is('dashboard/*'))
                    
                     <li class="nav-item {{Request::is('/dashboard') ? 'active' : ''}}">
                        <a class="nav-link" href="/dashboard">Administrateur <span class="sr-only">(current)</span></a>
                    </li>
                    @else
                    <li class="nav-item {{Request::is('/') ? 'active' : ''}}">
                        <a class="nav-link" href="/">Home <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="{{Request::is('news') ? 'active' : ''}}">
                        <a class="nav-link" href="/news">News</a>
                    </li>
                    <li class="{{Request::is('games') ? 'active' : ''}}">
                        <a class="nav-link" href="/games"> Games</a>
                    </li>
                    {{--  <li class="{{Request::is('about') ? 'active' : ''}}">
                        <a class="nav-link" href="/about">More</a>
                    </li>  --}}

                    <li class="{{Request::is('communautes') ? 'active' : ''}}">
                            <a class="nav-link" href="/communautes">Communautés</a>
                    </li>
                    <li><div class="dropdown">
                    <a class="nav-link" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        more
                    </a>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                       <div class="more-menu row-padding slide" style="height: 402px;">
                <div class="menu-content main-menu">
                    <div class="menu-side left">
                        <h1 class="more-menu-label">IGN Entertainment</h1>
                        <div class="cols">
                            <ul class="col">
                                <li class="col-link"><a href="http://corp.ign.com/about" class="link">About Us</a></li>
                                <li class="col-link"><a href="/support#3" class="link">Accessibility</a></li>
                                <li class="col-link"><a href="http://corp.ign.com" class="link">Advertise</a></li>
                                <li class="col-link"><a href="http://preferences-mgr.truste.com/?type=ziffdavispop&amp;pid=ziffdavis01&amp;aid=ziffdavis01" class="link">AdChoices <img class="link-logo ad-choice-logo" src="http://cdn.ziffstatic.com/adchoices/adchoices.png" alt="" hidden="" style="display: none !important;"></a></li>
                                <li class="col-link"><a href="http://corp.ign.com/careers" class="link">Careers</a></li>
                                <li class="col-link"><a href="/wikis/ign/Content_Team" class="link">Content Team</a></li>
                            </ul>
                            <ul class="col">
                                <li class="col-link"><a href="http://corp.ign.com/press" class="link">Press Room</a></li>
                                <li class="col-link"><a href="http://corp.ign.com/policies/privacy" class="link">Privacy Policy</a></li>
                                <li class="col-link"><a href="/sitemaps" class="link">Site Map</a></li>
                                <li class="col-link"><a href="/wikis/ign/Standards_and_Practices" class="link">Standards</a></li>
                                <li class="col-link"><a href="http://corp.ign.com/policies/user-agreement" class="link">Terms of Use</a></li>
                                <li class="col-link">
<div class="truste-logo-container">
    <a class="link" href="//privacy.truste.com/privacy-seal/validation?rid=e3a52610-3310-4ea5-b174-91a7833ae9c7" target="_blank">
        TRUSTe <img class="link-logo truste-logo" style="border: none" src="//privacy-policy.truste.com/privacy-seal/seal?rid=e3a52610-3310-4ea5-b174-91a7833ae9c7" alt="TRUSTe" width="52" height="16">
    </a>
</div>
</li>
                            </ul>
                        </div>
                        <div class="region">
                            <div class="region-selector">
        <select id="region-dropdown" class="region-dropdown">
                    <option value="" disabled="" selected="">Change Region</option> 
                <option value="http://world.ign.com/">World.IGN.com</option>
                <option value="http://m.ign.com/?setccpref=ZA">Africa</option>
                <option value="http://adria.ign.com/">Adria</option>
                <option value="http://m.ign.com/?setccpref=AU">Australia</option>
                <option value="http://m.ign.com/?setccpref=NL">Benelux</option>
                <option value="http://m.ign.com/?setccpref=BR">Brazil</option>
                <option value="http://m.ign.com/?setccpref=CA">Canada</option>
                <option value="http://m.ign.com/?setccpref=CN">China</option>
                <option value="http://m.ign.com/?setccpref=CZ">Czech / Slovakia</option>
                <option value="http://m.ign.com/?setccpref=FR">France</option>
                <option value="http://m.ign.com/?setccpref=DE">Germany</option>
                <option value="http://m.ign.com/?setccpref=GR">Greece</option>
                <option value="http://m.ign.com/?setccpref=HU">Hungary</option>
                <option value="http://m.ign.com/?setccpref=IN">India</option>
                <option value="http://m.ign.com/?setccpref=IE">Ireland</option>
                <option value="http://m.ign.com/?setccpref=IL">Israel</option>
                <option value="http://m.ign.com/?setccpref=IT">Italy</option>
                <option value="http://m.ign.com/?setccpref=JP">Japan</option>
                <option value="http://latam.ign.com/">Latin America</option>
                <option value="http://me.ign.com/en">Middle East - English</option>
                <option value="http://me.ign.com/ar">Middle East - Arabic</option>
                <option value="http://nordic.ign.com/">Nordic</option>
                <option value="http://m.ign.com/?setccpref=PK">Pakistan</option>
                <option value="http://m.ign.com/?setccpref=PL">Poland</option>
                <option value="http://m.ign.com/?setccpref=PT">Portugal</option>
                <option value="http://m.ign.com/?setccpref=RO">Romania</option>
                <option value="http://m.ign.com/?setccpref=RU">Russia</option>
                <option value="http://sea.ign.com/">Southeast Asia</option>
                <option value="http://m.ign.com/?setccpref=ES">Spain</option>
                <option value="http://m.ign.com/?setccpref=TR">Turkey</option>
                <option value="http://m.ign.com/?setccpref=UK">United Kingdom</option>
                <option value="http://m.ign.com/?setccpref=US">United States</option>
            </select>
</div>
                        </div>
                    </div>
                    <div class="menu-side right">
                        <div class="cols">
                            <ul class="col">
                                <div class="col-content">
                                    <h1 class="more-menu-label">Social</h1>
                                    <li class="col-link"><a href="/podcasts" class="link">Podcasts</a></li>
                                    <li class="col-link"><a href="http://www.youtube.com/ign" class="link">YouTube</a></li>
                                    <li class="col-link"><a href="http://www.ign.com/articles/2015/08/26/ign-is-coming-to-snapchat" class="link">Snapchat</a></li>
                                    <li class="col-link"><a href="https://www.facebook.com/ign/" class="link">Facebook</a></li>
                                    <li class="col-link"><a href="https://twitter.com/IGN" class="link">Twitter</a></li>
                                    <li class="col-link"><a href="https://www.instagram.com/igndotcom/" class="link">Instagram</a></li>
                                    <li class="col-link"><a href="https://www.twitch.tv/ign" class="link">Twitch</a></li>
                                </div>
                            </ul>
                            <ul class="col">
                                <div class="col-content">
                                    <h1 class="more-menu-label">Entertainment</h1>
                                    <li class="col-link"><a href="/movies" class="link">Movies</a></li>
                                    <li class="col-link"><a href="/tv" class="link">TV Shows</a></li>
                                    <li class="col-link"><a href="/comics" class="link">Comics</a></li>
                                    <li class="col-link"><a href="/tech" class="link">Tech</a></li>
                                    <li class="col-link"><a href="/trailers" class="link">Trailers</a></li>
                                </div>
                            </ul>
                            <ul class="col">
                                <div class="col-content">
                                    <h1 class="more-menu-label">Games</h1>
                                    <li class="col-link"><a href="/ps4" class="link">PlayStation</a></li>
                                    <li class="col-link"><a href="/xbox-one" class="link">Xbox</a></li>
                                    <li class="col-link"><a href="/nintendo" class="link">Nintendo</a></li>
                                    <li class="col-link"><a href="/pc" class="link">PC</a></li>
                                    <li class="col-link"><a href="/wireless" class="link">Mobile</a></li>
                                    <li class="col-link"><a href="/upcoming/games" class="link">Upcoming</a></li>
                                    <li class="col-link"><a href="/events/esports" class="link">eSports</a></li>
                                </div>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="menu-content legal">
                    <span class="legal-text">©1996-2017 Ziff Davis, LLC</span>
                </div>
            </div>
                    </div>
                    </div></li>
                    {{--  <li class="{{Request::is('messages') ? 'active' : ''}}">
                            <a class="nav-link" href="/messages">Messages</a>
                    </li>  --}}
                    <li class="{{Request::is('contact') ? 'active' : ''}}">
                        <a class="nav-link" href="/contact">Contact</a>
                    </li>
                    @endif
                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                    <!-- Authentication Links -->
                    @guest
                        <li><a href="{{ route('login') }}">Login</a></li>
                        <li><a href="{{ route('register') }}">Register</a></li>
                    @else

                        <li class="dropdown">
                                    
                            <a href="#" class="dropdown-toggle profile-img" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true">
                                    <img src="@if( !filter_var(Auth::user()->avatar , FILTER_VALIDATE_URL)){{ Voyager::image( Auth::user()->avatar  ) }}@else{{ Auth::user()->avatar  }}@endif"
                                    class="profile-img"
                                     alt="{{ Auth::user()->name }} avatar"><span class="caret"></span>
                                {{--  <img class="profile-img" src="/storage/{{ Auth::user()->avatar }} "><span class="caret"></span>  --}}
                            </a>

                            <ul class="dropdown-menu dropdown-menu-animated">
                                    <li class="profile-img">
                                        {{--  <input id="{{ Auth::user()->role_id }}" name="prodId" type="" value="{{ Auth::user()->role_id }}" hidden>  --}}
                                        <img src="@if( !filter_var(Auth::user()->avatar , FILTER_VALIDATE_URL)){{ Voyager::image( Auth::user()->avatar  ) }}@else{{ Auth::user()->avatar  }}@endif"
                                        class="profile-img-nav"
                                         alt="{{ Auth::user()->name }} avatar">
                                         {{--  <img class="profile-img" src="/storage/{{ Auth::user()->avatar }} ">  --}}
                                        <div class="profile-body">
                                                @if(Auth::user()->role_id == '1')
                                                <h5 style="color:red">{{ Auth::user()->name}}</h5>
                                                @elseif(Auth::user()->role_id == '2')
                                                <h5 style="color:blue">{{ Auth::user()->name}}</h5>
                                                @endif
                                            {{--  <h5>  {{ Auth::user()->name }}</h5>   --}}
                                            <h6>  {{ Auth::user()->email }}</h6>
                                        </div>
                                    </li>
                                <li class="divider"></li>
                                <li><a class="nav-link" href="/profile"><i class="fas fa-user"></i> My Profile</a></li>
                                {{--  <li><a class="nav-link" href="/panel">panel admins</a></li>  --}}
                                <li><a class="nav-link" href="/"><i class="fas fa-home"></i> Home</a></li>
                                    @if( Auth::user()->role_id == '1')
                                        <li><a class="nav-link" href="/admin" style="color:red"><i class="fas fa-tachometer-alt"></i> Admin</a></li>
                                        <li><a class="nav-link" href="/dashboard"><i class="fas fa-tachometer-alt"></i> dashboard membre</a></li>
                                    @endif
                                    @if(Auth::user()->role_id == '2s')
                                        <li><a class="nav-link" href="/dashboard">dashboard</a></li>
                                    @endif
                                    {{--@if( Auth::user()->role_id == '1')
                                    
                                    @endif  --}}
                                    <li><a class="btn btn-danger btn-block" href="{{ route('logout') }}"
                                        onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                        <i class="fas fa-sign-out-alt"></i> Logout
                                    </a></li>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                            </ul>
                        </li>
                    @endguest
                </ul>
            </div>
        </div>
    </nav>
  </header>