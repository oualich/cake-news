@section('navbar')
<header>
    <nav class="navbar navbar-default navbar-static-top">
        <div class="container-fluid">
            <div class="navbar-header">
                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse" aria-expanded="false">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Branding Image -->
                <a class="navbar-brand" href="{{ url('/') }}">
                    {{ config('app.name', 'Laravel') }}
                </a>
            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
                <ul class="nav navbar-nav">

                    @if (Request::is('dashboard') || Request::is('dashboard/*'))
                    
                     <li class="nav-item {{Request::is('/dashboard') ? 'active' : ''}}">
                        <a class="nav-link" href="/dashboard">Administrateur <span class="sr-only">(current)</span></a>
                    </li>
                    @else
                    <li class="nav-item {{Request::is('/') ? 'active' : ''}}">
                        <a class="nav-link" href="/">Home <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="{{Request::is('news') ? 'active' : ''}}">
                        <a class="nav-link" href="/news">News</a>
                    </li>
                    <li class="{{Request::is('games') ? 'active' : ''}}">
                        <a class="nav-link" href="/games"> Games</a>
                    </li>
                    {{--  <li class="{{Request::is('about') ? 'active' : ''}}">
                        <a class="nav-link" href="/about">More</a>
                    </li>  --}}
                    <div class="dropdown">
                    <button class=" nab-link dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        more
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <a class="dropdown-item" href="#">Action</a>
                        <a class="dropdown-item" href="#">Another action</a>
                        <a class="dropdown-item" href="#">Something else here</a>
                    </div>
                    </div>
                    <li class="{{Request::is('communautes') ? 'active' : ''}}">
                            <a class="nav-link" href="/communautes">Communautés</a>
                    </li>

                    {{--  <li class="{{Request::is('messages') ? 'active' : ''}}">
                            <a class="nav-link" href="/messages">Messages</a>
                    </li>  --}}
                    <li class="{{Request::is('contact') ? 'active' : ''}}">
                        <a class="nav-link" href="/contact">Contact</a>
                    </li>
                    @endif
                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                    <!-- Authentication Links -->
                    @guest
                        <li><a href="{{ route('login') }}">Login</a></li>
                        <li><a href="{{ route('register') }}">Register</a></li>
                    @else

                        <li class="dropdown">
                                    
                            <a href="#" class="dropdown-toggle profile-img" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true">
                                    <img src="@if( !filter_var(Auth::user()->avatar , FILTER_VALIDATE_URL)){{ Voyager::image( Auth::user()->avatar  ) }}@else{{ Auth::user()->avatar  }}@endif"
                                    class="profile-img"
                                     alt="{{ Auth::user()->name }} avatar"><span class="caret"></span>
                                {{--  <img class="profile-img" src="/storage/{{ Auth::user()->avatar }} "><span class="caret"></span>  --}}
                            </a>

                            <ul class="dropdown-menu dropdown-menu-animated">
                                    <li class="profile-img">
                                        {{--  <input id="{{ Auth::user()->role_id }}" name="prodId" type="" value="{{ Auth::user()->role_id }}" hidden>  --}}
                                        <img src="@if( !filter_var(Auth::user()->avatar , FILTER_VALIDATE_URL)){{ Voyager::image( Auth::user()->avatar  ) }}@else{{ Auth::user()->avatar  }}@endif"
                                        class="profile-img-nav"
                                         alt="{{ Auth::user()->name }} avatar">
                                         {{--  <img class="profile-img" src="/storage/{{ Auth::user()->avatar }} ">  --}}
                                        <div class="profile-body">
                                                @if(Auth::user()->role_id == '1')
                                                <h5 style="color:red">{{ Auth::user()->name}}</h5>
                                                @elseif(Auth::user()->role_id == '2')
                                                <h5 style="color:blue">{{ Auth::user()->name}}</h5>
                                                @endif
                                            {{--  <h5>  {{ Auth::user()->name }}</h5>   --}}
                                            <h6>  {{ Auth::user()->email }}</h6>
                                        </div>
                                    </li>
                                <li class="divider"></li>
                                <li><a class="nav-link" href="/profile"><i class="fas fa-user"></i> My Profile</a></li>
                                {{--  <li><a class="nav-link" href="/panel">panel admins</a></li>  --}}
                                <li><a class="nav-link" href="/"><i class="fas fa-home"></i> Home</a></li>
                                    @if( Auth::user()->role_id == '1')
                                        <li><a class="nav-link" href="/admin" style="color:red"><i class="fas fa-tachometer-alt"></i> Admin</a></li>
                                        <li><a class="nav-link" href="/dashboard"><i class="fas fa-tachometer-alt"></i> dashboard membre</a></li>
                                    @endif
                                    @if(Auth::user()->role_id == '2s')
                                        <li><a class="nav-link" href="/dashboard">dashboard</a></li>
                                    @endif
                                    {{--@if( Auth::user()->role_id == '1')
                                    
                                    @endif  --}}
                                    <li><a class="btn btn-danger btn-block" href="{{ route('logout') }}"
                                        onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                        <i class="fas fa-sign-out-alt"></i> Logout
                                    </a></li>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                            </ul>
                        </li>
                    @endguest
                </ul>
            </div>
        </div>
    </nav>
  </header>