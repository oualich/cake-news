@section('navbar')
<header>
<style>
.drop-more {
        width: 1143px;
            margin-left: -24em;


}
.nav-border-bottom {
        border-bottom: 2px solid #03A9F4;
}
</style>
    <nav class="navbar navbar-default navbar-static-top nav-border-bottom">
        <div class="container-fluid">
            <div class="navbar-header">
                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse" aria-expanded="false">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Branding Image -->
                <a class="navbar-brand" href="{{ url('/') }}">
                    {{ config('app.name', 'Laravel') }}
                </a>
            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
                <ul class="nav navbar-nav">

                    @if (Request::is('dashboard') || Request::is('dashboard/*'))
                    
                     <li class="nav-item {{Request::is('/dashboard') ? 'active' : ''}}">
                        <a class="nav-link" href="/dashboard">Administrateur <span class="sr-only">(current)</span></a>
                    </li>
                    @else
                    <li class="nav-item {{Request::is('/') ? 'active' : ''}}">
                        <a class="nav-link" href="/">Home <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="{{Request::is('news') ? 'active' : ''}}">
                        <a class="nav-link" href="/news">World</a>
                    </li>
                    <li class="{{Request::is('games') ? 'active' : ''}}">
                        <a class="nav-link" href="/games"> High-Tech</a>
                    </li>
                    <li class="{{Request::is('games') ? 'active' : ''}}">
                        <a class="nav-link" href="/games"> Science</a>
                    </li>
                    {{--  <li class="{{Request::is('about') ? 'active' : ''}}">
                        <a class="nav-link" href="/about">More</a>
                    </li>  --}}

                    <li class="{{Request::is('communautes') ? 'active' : ''}}">
                            <a class="nav-link" href="/communautes">Communautés</a>
                    </li>
                    <li class="dropdown">
                        <a class="nav-link" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            More
                        </a>
                        <div class="dropdown-menu drop-more" aria-labelledby="dropdownMenuButton">
                        <div class="col-md-4">
                                <h1>blog</h1>
                                <a class="dropdown-item" href="#">About US</a><br>
                                <a class="dropdown-item" href="#">Contacts US</a><br>
                                <a class="dropdown-item" href="#">ANNONCES</a>
                            </div>
                            <div class="col-md-4">
                                <h1>sponsors</h1>
                                <a class="dropdown-item" href="#">NASA</a><br>
                                <a class="dropdown-item" href="#">Google</a><br>
                                <a class="dropdown-item" href="#">Microsoft</a><br>
                                <a class="dropdown-item" href="#">Sony</a><br>
                                <a class="dropdown-item" href="#">Nintendo</a>
                            </div>
                            <div class="col-md-4">
                                <h1>social</h1>
                                <a class="dropdown-item" href="#">Youtube</a><br>
                                <a class="dropdown-item" href="#">Facebook</a><br>
                                <a class="dropdown-item" href="#">Dailymotion</a><br>
                                <a class="dropdown-item" href="#">Instagram</a>
                            </div>
                        </div>
                    
                    </li>
                    {{--  <li class="{{Request::is('messages') ? 'active' : ''}}">
                            <a class="nav-link" href="/messages">Messages</a>
                    </li>  --}}
                    <li class="{{Request::is('contact') ? 'active' : ''}}">
                        <a class="nav-link" href="/contact">Contact US</a>
                    </li>
                    @endif
                </ul>

                <!-- Right Side Of Navbar -->
                    {!! Form::open(['method'=>'GET','url'=>'games','class'=>'navbar-form navbar-left','role'=>'search'])  !!}                       
                        <div class="input-group custom-search-form">
                            <input type="text" class="form-control" name="search" placeholder="Search...">
                            <span class="input-group-btn">
                            <button class="btn btn-default-sm" type="submit"><i class="fa fa-search">recherche</button>
                            </span>
                        </div>        
                    {!! Form::close() !!}
                <ul class="nav navbar-nav navbar-right">

                    <!-- Authentication Links -->
                    @guest
                        <li><a href="{{ route('login') }}">Login</a></li>
                        <li><a href="{{ route('register') }}">Register</a></li>
                    @else

                        <li class="dropdown">
                                    
                            <a href="#" class="dropdown-toggle profile-img" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true">
                                    <img src="@if( !filter_var(Auth::user()->avatar , FILTER_VALIDATE_URL)){{ Voyager::image( Auth::user()->avatar  ) }}@else{{ Auth::user()->avatar  }}@endif"
                                    class="profile-img"
                                     alt="{{ Auth::user()->name }} avatar"><span class="caret"></span>
                                {{--  <img class="profile-img" src="/storage/{{ Auth::user()->avatar }} "><span class="caret"></span>  --}}
                            </a>

                            <ul class="dropdown-menu dropdown-menu-animated">
                                    <li class="profile-img">
                                        {{--  <input id="{{ Auth::user()->role_id }}" name="prodId" type="" value="{{ Auth::user()->role_id }}" hidden>  --}}
                                        <img src="@if( !filter_var(Auth::user()->avatar , FILTER_VALIDATE_URL)){{ Voyager::image( Auth::user()->avatar  ) }}@else{{ Auth::user()->avatar  }}@endif"
                                        class="profile-img-nav"
                                         alt="{{ Auth::user()->name }} avatar">
                                         {{--  <img class="profile-img" src="/storage/{{ Auth::user()->avatar }} ">  --}}
                                        <div class="profile-body">
                                                @if(Auth::user()->role_id == '1')
                                                <h5 style="color:red">{{ Auth::user()->name}}</h5>
                                                @elseif(Auth::user()->role_id == '2')
                                                <h5 style="color:blue">{{ Auth::user()->name}}</h5>
                                                @endif
                                            {{--  <h5>  {{ Auth::user()->name }}</h5>   --}}
                                            <h6>  {{ Auth::user()->email }}</h6>
                                        </div>
                                    </li>
                                <li class="divider"></li>
                                <li><a class="nav-link" href="/profile"><i class="fas fa-user"></i> My Profile</a></li>
                                {{--  <li><a class="nav-link" href="/panel">panel admins</a></li>  --}}
                                <li><a class="nav-link" href="/"><i class="fas fa-home"></i> Home</a></li>
                                    @if( Auth::user()->role_id == '1')
                                        <li><a class="nav-link" href="/admin" style="color:red"><i class="fas fa-tachometer-alt"></i> Admin</a></li>
                                        <li><a class="nav-link" href="/dashboard"><i class="fas fa-tachometer-alt"></i> dashboard membre</a></li>
                                    @endif
                                    @if(Auth::user()->role_id == '2s')
                                        <li><a class="nav-link" href="/dashboard">dashboard</a></li>
                                    @endif
                                    {{--@if( Auth::user()->role_id == '1')
                                    
                                    @endif  --}}
                                    <li><a class="btn btn-danger btn-block" href="{{ route('logout') }}"
                                        onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                        <i class="fas fa-sign-out-alt"></i> Logout
                                    </a></li>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                            </ul>
                        </li>
                    @endguest
                </ul>
            </div>
        </div>
    </nav>
  </header>