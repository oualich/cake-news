@section('navbar')
<header>
<style>
.drop-more {
        width: 1143px;
            margin-left: -24em;


}
.nav-border-bottom {
        border-bottom: 2px solid #03A9F4;
}
</style>
    <nav class="navbar navbar-default navbar-static-top nav-border-bottom">
        <div class="container-fluid">
            <div class="navbar-header">
                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse" aria-expanded="false">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Branding Image -->
                <a class="navbar-brand" href="{{ url('/') }}">
                    {{ config('app.name', 'Laravel') }}
                </a>
            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
                <ul class="nav navbar-nav">

                    @if (Request::is('dashboard') || Request::is('dashboard/*'))
                    
                     <li class="nav-item {{Request::is('/dashboard') ? 'active' : ''}}">
                        <a class="nav-link" href="/dashboard">Administrateur <span class="sr-only">(current)</span></a>
                    </li>
                    @else
                    <li class="nav-item border-navbar-left {{Request::is('/') ? 'active' : ''}}">
                        <a class="nav-link" href="/">Home <span class="sr-only">(current)</span></a>
                    </li>
                    <li class=" border-navbar-left {{Request::is('news') ? 'active' : ''}}">
                        <a class="nav-link" href="/news">World</a>
                    </li>
                    <li class=" border-navbar-left {{Request::is('games') ? 'active' : ''}}">
                        <a class="nav-link" href="/games"> High-Tech</a>
                    </li>
                    <li class=" border-navbar-left {{Request::is('games') ? 'active' : ''}}">
                        <a class="nav-link" href="/games"> Science</a>
                    </li>
                    {{--  <li class="{{Request::is('about') ? 'active' : ''}}">
                        <a class="nav-link" href="/about">More</a>
                    </li>  --}}
                    <li class="dropdown">
                        <a class="nav-link" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            search
                        </a>
                        <div class="dropdown-menu drop-more" aria-labelledby="dropdownMenuButton">
                            <div class="modal search-modal " style="width: 515px; position: fixed; margin-left: 0px; margin-top: 0px; top: 67px; left: 236.734px;">
                                    <div class="modal-header">
                                        <h5 class="modal-heading">Search</h5>            
                                    </div>
                                    <div class="modal-content">
                                        <form class="search-form-control form-control" action="https://query.nytimes.com/search/video?module=search-popup&amp;action=submit&amp;region=modal&amp;version=times-video&amp;contentCollection=Library&amp;pgType=Multimedia" method="get" role="search" id="search-form">
                                <fieldset>
                                    <div class="input-embed-button">
                                    <input name="query" maxlength="65" class="search-input text" type="input" placeholder="Type what you're looking for here">
                                    <button class="embed-button">Go</button>
                                    </div>
                                    <div class="radio-group">
                                    <input id="times-video-checkbox" type="radio" name="type" value="times-video" class="radio-btn" checked="">
                                    <label id="times-video-label" for="times-video-checkbox" class="push-right-20 label">Only TimesVideo</label>
                                    <input id="nytimes-checkbox" type="radio" name="type" value="nytimes" class="radio-btn">
                                    <label id="nytimes-label" for="nytimes-checkbox" class="label">Search nytimes.com</label>
                                    </div>
                                </fieldset>
                                </form>
                                    
                                    </div>
                                    <button type="button" class="modal-close hidden"><i class="icon"></i><span class="visually-hidden">Close this modal window</span></button>
                                    <div class="modal-pointer modal-pointer-up-left"><div class="modal-pointer-conceal"></div></div>
                                </div>
                        </div>
                    
                    </li>
                    <li class="dropdown">
                        <a class="nav-link" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            More
                        </a>
                        <div class="dropdown-menu drop-more" aria-labelledby="dropdownMenuButton">
                        <div class="col-md-4">
                                <h1>Cake-News</h1>
                                <a class="dropdown-item" href="#">About US</a><br>
                                <a class="dropdown-item" href="/contact">Contacts US</a><br>
                                <a class="dropdown-item" href="#">ANNONCES</a><br>

                                <a class="dropdown-item" href="/communautes">member</a>

                            </div>
                            <div class="col-md-4">
                                <h1>sponsors</h1>
                                <a class="dropdown-item" href="#">NASA</a><br>
                                <a class="dropdown-item" href="#">Google</a><br>
                                <a class="dropdown-item" href="#">Microsoft</a><br>
                                <a class="dropdown-item" href="#">Sony</a><br>
                                <a class="dropdown-item" href="#">Nintendo</a>
                            </div>
                            <div class="col-md-4">
                                <h1>social</h1>
                                <a class="dropdown-item" href="#">Youtube</a><br>
                                <a class="dropdown-item" href="#">Facebook</a><br>
                                <a class="dropdown-item" href="#">Dailymotion</a><br>
                                <a class="dropdown-item" href="#">Instagram</a>
                            </div>
                        </div>
                    
                    </li>
                    {{--  <li class="{{Request::is('messages') ? 'active' : ''}}">
                            <a class="nav-link" href="/messages">Messages</a>
                    </li>  --}}
                    {{-- <li class="{{Request::is('contact') ? 'active' : ''}}">
                        <a class="nav-link" href="/contact">Contact US</a>
                    </li> --}}
                                    {!! Form::open(['method'=>'GET','url'=>'search','class'=>'navbar-form navbar-right inline-form','role'=>'search'])  !!}                       

            <div class="form-group">
              <input type="search" class="input-sm form-control" name="search" placeholder="Search...">
            </div>
          {!! Form::close() !!}
                    @endif
                </ul>

                <!-- Right Side Of Navbar -->

                <ul class="nav navbar-nav navbar-right border-navbar-left">

                    <!-- Authentication Links -->
                    @guest
                        <li><a href="{{ route('login') }}">Login</a></li>
                        <li><a href="{{ route('register') }}">Register</a></li>
                    @else

                        <li class="dropdown">
                                    
                            <a href="#" class="dropdown-toggle profile-img" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true">
                                    <img src="@if( !filter_var(Auth::user()->avatar , FILTER_VALIDATE_URL)){{ Voyager::image( Auth::user()->avatar  ) }}@else{{ Auth::user()->avatar  }}@endif"
                                    class="profile-img"
                                     alt="{{ Auth::user()->name }} avatar"><span class="caret"></span>
                                {{--  <img class="profile-img" src="/storage/{{ Auth::user()->avatar }} "><span class="caret"></span>  --}}
                            </a>

                            <ul class="dropdown-menu dropdown-menu-animated">
                                    <li class="profile-img">
                                        {{--  <input id="{{ Auth::user()->role_id }}" name="prodId" type="" value="{{ Auth::user()->role_id }}" hidden>  --}}
                                        <img src="@if( !filter_var(Auth::user()->avatar , FILTER_VALIDATE_URL)){{ Voyager::image( Auth::user()->avatar  ) }}@else{{ Auth::user()->avatar  }}@endif"
                                        class="profile-img-nav"
                                         alt="{{ Auth::user()->name }} avatar">
                                         {{--  <img class="profile-img" src="/storage/{{ Auth::user()->avatar }} ">  --}}
                                        <div class="profile-body">
                                                @if(Auth::user()->role_id == '1')
                                                <h5 style="color:red">{{ Auth::user()->name}}</h5>
                                                @elseif(Auth::user()->role_id == '2')
                                                <h5 style="color:blue">{{ Auth::user()->name}}</h5>
                                                @endif
                                            {{--  <h5>  {{ Auth::user()->name }}</h5>   --}}
                                            <h6>  {{ Auth::user()->email }}</h6>
                                        </div>
                                    </li>
                                <li class="divider"></li>
                                <li><a class="nav-link" href="{{ route('account.index') }}"><i class="fas fa-user"></i> My Profile</a></li>
                                {{--  <li><a class="nav-link" href="/panel">panel admins</a></li>  --}}
                                <li><a class="nav-link" href="{{ route('home') }}"><i class="fas fa-home"></i> Home</a></li>
                                    @if( Auth::user()->role_id == '1')
                                        <li><a class="nav-link" href="{{ route('voyager.dashboard') }}" style="color:red"><i class="fas fa-tachometer-alt"></i> Admin</a></li>
                                        <li><a class="nav-link" href="{{ route('dashboard') }}"><i class="fas fa-tachometer-alt"></i> dashboard membre</a></li>
                                    @endif
                                    @if(Auth::user()->role_id == '2s')
                                        <li><a class="nav-link" href="/dashboard">dashboard</a></li>
                                    @endif
                                    {{--@if( Auth::user()->role_id == '1')
                                    
                                    @endif  --}}
                                    <li><a class="btn btn-danger btn-block" href="{{ route('logout') }}"
                                        onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                        <i class="fas fa-sign-out-alt"></i> Logout
                                    </a></li>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                            </ul>
                        </li>

                    @endguest
                    
                </ul>

            </div>
        </div>
    </nav>
  </header>