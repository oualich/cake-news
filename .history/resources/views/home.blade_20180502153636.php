@extends('layouts.app')
@section('title')
    Home 
@endsection
@section('script')
<script defer src="https://use.fontawesome.com/releases/v5.0.8/js/all.js"></script>
@endsection

@section('content')
    <div class="container-fluid">        
        <div class="row">
            <div class="col-md-10 col-md-offset-2">
                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                </div>
            </div>

            <section id="news">
                    <div class="bord_rubrique">
                        <h1 class="entete_deroule tt5">News World</h1>
                    </div>
                    @if(count($posts) > 0)
                    <div class="container-fluid news-blog">
                        <div class="row">
                        @foreach ($posts->take(8) as $post)
                            {{-- <p> Posted by <strong> {{ $post->category->name}} </strong> </p> --}}

                            {{-- condition pour afficher la categorie  --}}
                            @if ($post->category->name == "News")
                            {{--  {{ $post->category_id = str_replace('1', ' actualite', $post->category_id) }}  --}}
                                
                                                {{-- condition pour afficher un article publier  --}}                              
                                <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 col-xl-3">
                                    <div class="div-article-home">
                                        <div class="post-image" style="background:url('@if( !filter_var($post->image, FILTER_VALIDATE_URL)){{ Voyager::image( $post->image ) }}@else{{ $post->image }}@endif')">
                                            
                                            <div class="post-author">{{ $post->category->name}}</div>
                                        </div>
                                        <div class="post-card">
                                            {{--  <div class="post-cat">{{ $post->category->name}}</div>  --}}
                                            <a href="post/{{ $post->id }}-{{ $post->slug }}/" class="post-title">{!!$post->title!!}</a>
                                            <div class="post-date">{{ $post->created_at}}</div>
                                            <div class="post-content">
                                                <p>{{ $post->description }} </p>
                                            </div>
                                            <a href="post/{{ $post->id }}-{{ $post->slug }}/" class="post-read">Read More</a>

                                        </div> 
                                    </div> 
                                </div>
                            @else
                            @endif
                        @endforeach  
                        </div>
                    </div>
                        <div class="container-fluid">
                            <div class="row text-center">
                                <a href="#" class="btn btn-outline-primary text-center" style="border: 1px solid;">  Read more news</a>       
                            </div>
                        </div>
                            @else
                                <h1><strong>aucun article na encore etait publier</strong></h1>
                            @endif
            </section>
                    
            <section id="jeux">
                <div class="row">
                    <div class="bord_rubrique">
                        <h1 class="entete_deroule tt5">Science/High-Tech</h1>
                    </div>
                    @if(count($games) > 0)
                    
                        @foreach ($games->take(8) as $post)
                            {{-- condition pour afficher la categorie  --}}
                            @if ($post->category->name == "Science")                        
                                {{--  {{ $post->category_id = str_replace('1', ' actualite', $post->category_id) }}  --}}
                                
                                {{-- condition pour afficher un article publier  --}}
                            
                                
                                <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 col-xl-3">
                                    <div class="div-article-home">
                                        <div class="post-image" style="background:url('@if( !filter_var($post->image, FILTER_VALIDATE_URL)){{ Voyager::image( $post->image ) }}@else{{ $post->image }}@endif')">
                                            
                                            <div class="post-author">{{ $post->category->name}}</div>
                                        </div>
                                        <div class="post-card">
                                            {{--  <div class="post-cat">{{ $post->category->name}}</div>  --}}
                                            <a href="post/{{ $post->id }}-{{ $post->slug }}/" class="post-title">{!!$post->title!!}</a>
                                            <div class="post-date">{{ $post->created_at}}</div>
                                            <div class="post-content">
                                                <p>{{ $post->description }} </p>
                                            </div>
                                            <a href="post/{{ $post->id }}-{{ $post->slug }}/" class="post-read">Read More</a>

                                        </div> 
                                    </div> 
                                </div>
                            @else
                            @endif

                        @endforeach  

                        <div class="container-fluid">
                            <div class="row text-center">
                                <a href="#" class="btn btn-outline-primary text-center" style="border: 1px solid;">  Read more news</a>       
                            </div>
                        </div>
                    @else
                        <h1><strong>aucun article na encore etait publier</strong></h1>
                    @endif
                </div>
            </section>
        </div>    
    </div>
    
@endsection
{{--  @auth
    // The user is authenticated...
@endauth

@guest
    // The user is not authenticated...
@endguest  --}}