@extends('layouts.app')
@section('title')
    Home 
@endsection
@section('script')
<script defer src="https://use.fontawesome.com/releases/v5.0.8/js/all.js"></script>
@endsection

@section('content')
    <div class="container-fluid">        
        <div class="row">
            <div class="col-md-10 col-md-offset-2">
                <div class="">
                        <div class="panel-body">
                            @if (session('status'))
                                <div class="alert alert-success">
                                    {{ session('status') }}
                                </div>
                            @endif
                        </div>
                </div>
            </div>
<style>

</style>
            <div id="news">
                <div class="bord_rubrique">
                    <h1 class="entete_deroule tt5">News World</h1>
                </div>
                @if(count($posts) > 0)

                    @foreach ($posts->take(8) as $post)
                        {{-- <p> Posted by <strong> {{ $post->category->name}} </strong> </p> --}}

                        {{-- condition pour afficher la categorie  --}}
                        @if ($post->category->name == "News")
                        {{--  {{ $post->category_id = str_replace('1', ' actualite', $post->category_id) }}  --}}
                               
                                             {{-- condition pour afficher un article publier  --}}                            

<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<div class="container-fluid news-blog">
    <div class="row">
       <div class="col-lg-4">
          <div class="page-header">
             <h1>Blog <small>Debate </small></h1>
          </div>
          <div class="col-sm-6 col-md-6">
             <a href="#">
                <div class="thumbnail principal-post">
                   <img src="https://i0.wp.com/radio.ufpa.br/wp-content/uploads/2017/09/Morador%20de%20rua.jpg">
                   <div class="caption">
                      <h2>População em situação de rua</h2>
                      <span class="date-of-post">4 de dezembro de 2017</span>
                      <p>De acordo com a pesquisa “Trabalho Docente e Saúde: tensões da Educação Superior”, entre 2006 e 2010, 14,13% dos pedidos de afastamento do trabalho dos professores da UFPA estiveram relacionados à saúde mental. Depressão e síndrome de Burnout estão entre os problemas mais frequentes apontados no trabalho realizado por Jadir Campos, no Programa de Pós-Graduação em Educação.</p>
                   </div>
                </div>
             </a>
          </div>
       </div>
   </div>
</div>
                        @else
                        @endif
                    @endforeach  
                    <div class="col-md-8 col-offset-2">
                        <a class="btn btn-primary btn-sm text-center">  Read more news</a>       
                    </div>
                @else
                    <h1><strong>aucun article na encore etait publier</strong></h1>
                @endif
            </div>
        </div>
        <div class="row">
                    
            <div id="jeux">
                <div class="bord_rubrique">
                    <h1 class="entete_deroule tt5">Science/High-Tech</h1>
                </div>
                @if(count($games) > 0)
                
                    @foreach ($games->take(8) as $post)
                    {{-- condition pour afficher la categorie  --}}
                        @if ($post->category->name == "Science")                        
                        {{--  {{ $post->category_id = str_replace('1', ' actualite', $post->category_id) }}  --}}
                            
                            {{-- condition pour afficher un article publier  --}}
                        
                            
                                <div class="col-md-3 col-lg-3 box-home">
                                    <ul class="col" data-id="{{$post->id}}">                        
                                                    {{--  choix 1  --}}
                                            <li class="bcolor">
                                                <a href="post/{{ $post->id }}/">
                                                    <img src="@if( !filter_var($post->image, FILTER_VALIDATE_URL)){{ Voyager::image( $post->image ) }}@else{{ $post->image }}@endif"
                                                            class="img-responsive img-height"
                                                            alt="{{ $post->title }} avatar">
                                                        <h3>{!!$post->title!!}</h3>
                                                        <p class="">{!!$post->excerpt!!}</p>
                                                    </a>
                                                    {{--  <span class="label">{!!$post->body!!}</span>  --}}
                                                    <a href="#jeux" class="tag">{{ $post->category->name }} </a>
                                            </li>
                                    </ul>
                                </div>
                                        @else
                        @endif

                    @endforeach  
                    <div class="col-md-8 col-offset-2">
                        <a class="btn btn-primary btn-sm text-center"> Read more news</a>       
                    </div> 
                @else
                    <h1><strong>aucun article na encore etait publier</strong></h1>
                @endif
            </div>
        </div>
           
    </div>
    
@endsection
{{--  @auth
    // The user is authenticated...
@endauth

@guest
    // The user is not authenticated...
@endguest  --}}