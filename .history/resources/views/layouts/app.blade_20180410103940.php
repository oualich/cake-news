<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('title')</title>
    @yield('css_page')
    @yield('script')
    {{--  <script defer src="https://use.fontawesome.com/releases/v5.0.8/js/all.js"></script>  --}}
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="/css/app.css">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <script src="{{ asset('js/app.css') }}"></script>
    <style>

    </style>
</head>
<body>
    @include('inc.navbar')      

    <section id="maincontent">
        <div class="container-fluid">
            <div class="row">
                <section class="slider-home">
                    <div class="container-fluid">
                        @if(Request::is('/'))
                        
                       <div class="col-xs|sm|md|lg|xl-1-12 masthead">
                            <ul class="masthead-menu">
                                <li class="date">{{date('l \\, j  F Y ')}}</li>
                                <li class="todays-paper"><a href="http://www.nytimes.com/pages/todayspaper/index.html" data-collection="todays-paper"><i class="icon sprite-icon"></i>Today’s Paper</a></li>
                                <li class="video"><a href="https://www.nytimes.com/video" data-collection="video"><i class="icon sprite-icon"></i>Video</a></li>
                            </ul>
                       </div>

                        
                            <div class="row">
                        
                                <div class="col-md-8 col-lg-8">
                                    @include('inc.showcase')
                                </div>
                                <div class="col-md-2 col-lg-2 col-md-offset-2">
                                    @include('inc.sidebare')
                                </div>
                                <div class="col-md-3 col-lg-3 col-md-offset-9">
                                    @include('inc.sidebare')
                                </div>
                            </div>
                            
                        @endif  
                    </div>
                </section>
                @include('inc.message')
                @yield('content')
                
                @if(Request::is('/'))

                @endif  
            </div>
        </div>
    </section>
    <footer id="footer" class="text-center">
        <p> Copyright 2018 &copy; Oualich</p>
    </footer>

</body>
</html>