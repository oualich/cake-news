@extends('layouts.app')
@if (Auth::user())
@section('title')
    dashboard 
@endsection
@section('script')
<script defer src="https://use.fontawesome.com/releases/v5.0.8/js/all.js"></script>
@endsection
@section('content')
            <div class="col-md-12">
                <div id="main-content">
                    <div id="main-wrapper">
                        <div id="col-left">
                            <ul>
                                <li class="title-menu">MENU</li>
                                <li id="dashboard" class="active"><i class="fas fa-tachometer-alt fa-lg"></i><a href="{{ route('dashboard') }}">Tableau de bord</a></li>
                                <li id="membres"><i class="fas fa-users fa-lg"></i><a href="{{ route('dashboard.member') }}"> Member</a></li>
                                <li id="publications"><i class="far fa-newspaper fa-lg"></i><a href="{{ route('dashboard.posts') }}"> Actualités</a></li>
                            </ul>
                            <ul>
                                <li class="title-menu">FONCTIONNALITÉS</li>
                                {{-- <li id="ajoutMembre"><i class="fa fa-plus-circle fa-lg"></i> Ajouter un membre</li> --}}
                                <li id="ajoutArticle" ><i class="fa fa-plus-circle fa-lg"></i><a href="{{ route('dashboard.posts.create') }}"> Ajouter une actualité </a></li>
                            </ul>
                            <ul>
                                <li class="title-menu">RÉGLAGES</li>
                                <li id="compte"><i class="fa fa-cog fa-lg"></i><a href="{{ route('account.index') }}"> Mon compte</a></li>
                                <li id="index"><i class="fa fa-power-off fa-lg"></i><a href="{{ route('logout') }}"  onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();"> Déconnexion</a></li>
                            </ul>
                        </div>
                        {{--  <a class="nav-link btn btn-success btn-add-new" href="post/create"><i class="voyager-plus"></i>Add New</a>  --}}
                            <div class="col-md-4">
                                <div class="panel widget center bgimage" style="margin-bottom:0;overflow:hidden;background-image:url('http://127.0.0.1:8000/vendor/tcg/voyager/assets/images/widget-backgrounds/02.jpg');">
                                    <div class="dimmer"></div>
                                    <div class="panel-content">
                                        <i class="voyager-news"></i><h4>{{ $posts->count() }} Posts</h4>
                                        <p>You have 1 post in your database. Click on button below to view all posts.</p>
                                        <a href="{{ route('dashboard.posts') }}" class="btn btn-primary">View all posts</a>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="panel widget center bgimage" style="margin-bottom:0;overflow:hidden;background-image:url('http://127.0.0.1:8000/vendor/tcg/voyager/assets/images/widget-backgrounds/02.jpg');">
                                    <div class="dimmer"></div>
                                    <div class="panel-content">
                                        <i class="voyager-news"></i><h4>{{ count(Auth::user()) }} users</h4>
                                        <p>You have 1 post in your database. Click on button below to view all posts.</p>
                                        <a href="{{ route('dashboard.member') }}" class="btn btn-primary">View all posts</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                        @section('sidebar')
                            @parent
                            <p>this is appended to the sidebar</p>
                        @endsection  
                        

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@else
        <script>window.location.href = "/login";</script>   
@endif