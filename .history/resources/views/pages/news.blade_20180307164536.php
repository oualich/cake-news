@extends('layouts.app')
@section('content')
    <div class="container">        
        <div class="row">
            <div class="col-md-10 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Dashboard</div>
                        <div class="panel-body">
                            @if (session('status'))
                                <div class="alert alert-success">
                                    {{ session('status') }}
                                </div>
                            @endif
                        </div>
                </div>
            </div>
            
            @section('content')
            
            <div class="col-md-12">
            <span>
                                {!! Form::open(['method'=>'GET','url'=>'news','class'=>'navbar-form navbar-left','role'=>'search'])  !!}
                           <div class="input-group custom-search-form">
                                <input type="text" class="form-control" name="search" placeholder="Search...">
                                <span class="input-group-btn">
                    <button class="btn btn-default-sm" type="submit">
                        <i class="fa fa-search">i
                    </button>
                </span>
                {!! Form::close() !!}
                </div>
                <div id="news">

                <h1>News</h1>
                
                    @if(count($posts) > 0)
                        @foreach ($posts as $post)

                            {{-- condition pour afficher la categorie  --}}
                            
                            @if ($post->category_id == '1' && $post->category_id = str_replace('1', 'Actualite', $post->category_id))
                            {{--  {{ $post->category_id = str_replace('1', ' actualite', $post->category_id) }}  --}}

                                {{-- condition pour afficher un article publier  --}}
                                @if ($post->status == 'PUBLISHED')
                                
                                    <div class="col-md-3 col-lg-3">
                                        <ul class="col" data-id="{{$post->id}}">                        
                                                        {{--  choix 1  --}}
                                                <li class="bcolor">
                                                    <a href="post/{{ $post->id }}/">
                                                        <img src="@if( !filter_var($post->image, FILTER_VALIDATE_URL)){{ Voyager::image( $post->image ) }}@else{{ $post->image }}@endif"
                                                                class="img-responsive img-height"
                                                                alt="{{ $post->title }} avatar">
                                                            <h3>{!!$post->title!!}</h3>
                                                            <p class="">{!!$post->excerpt!!}</p>
                                                        </a>
                                                        {{--  <span class="label">{!!$post->body!!}</span>  --}}
                                                        <a href="#news" class="tag">{{ $post->category_id}} </a>
                                                </li>
                                        </ul>
                                    </div>
                    
                                @endif        
                            @else
                            @endif
                        @endforeach   
 
                    @else
                        <h1><strong>aucun article na encore etait publier</strong></h1>
                    @endif
                </div>
                    </div>
                                                                                        <div class="panel-heading" style="display:flex; justify-content:center;align-items:center;">
                                                    {{$posts->links()}}
                                                </div>  
            @endsection
        </div>
    </div>
@endsection
{{--  @auth
    // The user is authenticated...
@endauth

@guest
    // The user is not authenticated...
@endguest  --}}