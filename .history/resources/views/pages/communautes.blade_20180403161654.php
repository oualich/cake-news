@extends('layouts.app')
@section('title')
    communautes
@endsection
@section('script')
<script defer src="https://use.fontawesome.com/releases/v5.0.8/js/all.js"></script>

@endsection
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    @section('content')
                        <div class="col-sm-12">
                        @if(count($users) > 0)

                            
                            <table class="table table-hover dataTable no-footer">
                                    <thead>
                                      <tr>
                                        <th>id</th>
                                        <th>nom</th>
                                        <th>Email</th>
                                        <th>Avatar</th>
                                        <th>inscrit depuis</th>

                                      </tr>
                                    </thead>
                                    @foreach ($users as $user)
                                    <tbody>
                                      <tr>
                                        <td>{!!$user->id!!}</td>
                                        @if($user->role_id == '1')
                                        <td style="color:red">{!!$user->name!!}</td>
                                        @elseif($user->role_id == '2')
                                        <td style="color:blue">{!!$user->name!!}</td>
                                        @endif
                                        <td>{!!$user->email!!}</td>
                                        <td> <img class="profile-img" src="./storage/{!!$user->avatar!!}"> </td>
                                        <td>{!!$user->created_at!!}</td>
                                      </tr>      
                                    </tbody>
                                    @endforeach
                                  </table>
                            
                            
                        
                            @endif
                            </div>
                        @endsection
                    
                    {{--  @section('sidebar')
                        @parent
                        <p>this is appended to the sidebar</p>
                    @endsection  --}}
                    

                </div>
            </div>
        </div>
    </div>
</div>

@endsection
