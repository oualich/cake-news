@extends('layouts.app')

@section('content')
<div class="container">
    <h1>Contact</h1>
    {!! Form::open(['url' => 'contact/submit']) !!}
    <div class="form-group">
            {{Form::label('name', 'Name')}}
           <br>
            {{Form::text('name', '', ['class' =>'form-control','placeholder'=>'enter name'])}}
        </div>   
    <div class="form-group">
        {{Form::label('Email', 'E-Mail Address')}}
       <br>
        {{Form::text('Email', '', ['class' =>'form-control','placeholder'=>'enter E-Mail Address'])}}
    </div>
    <div class="form-group">
        {{Form::label('Message', 'Message')}}
       <br>
        {{Form::textarea('message', '', ['class' =>'form-control','placeholder'=>'enter Message'])}}
    </div>      
    <div class="form-group">
            {{Form::submit('Submit', ['class' =>'btn btn-primary'])}}
    </div>  
    {!! Form::close() !!}
</div>


@endsection