@extends('layouts.app')
@section('title')
    News 
@endsection
@section('content')
    <div class="container-fluid">        
        <div class="row">
            <div class="col-md-12">
                {!! Form::open(['method'=>'GET','url'=>'news','class'=>'navbar-form navbar-left','role'=>'search'])  !!}                       
                    <div class="input-group custom-search-pages">
                        <input type="text" class="form-control" name="search" placeholder="Search..." size="80">
                        <span class="input-group-btn">
                        <button class="btn btn-default-sm" type="submit"><i class="fa fa-search">recherche</button>
                        </span>
                    </div>        
                {!! Form::close() !!}
            </div>
        
        <div id="news">
                <h1>Search</h1>
                <div class="col-xs|sm|md|lg|xl-4-4">
                    
                </div>
                    @if(count($posts) > 0)
                        @foreach ($posts as $post)

                            {{-- condition pour afficher la categorie  --}}
                            
                            {{--  {{ $post->category_id = str_replace('1', ' actualite', $post->category_id) }}  --}}

                                {{-- condition pour afficher un article publier  --}}
                                
                            <div class="col-lg-8  col-md-8 col-sm-4 col-xs-12 col-xl-8">
                                <div class="div-article-home">
                                    <div class="post-image" style="background:url('@if( !filter_var($post->image, FILTER_VALIDATE_URL)){{ Voyager::image( $post->image ) }}@else{{ $post->image }}@endif')">
                                        
                                        <div class="post-author">{{ $post->category->name}}</div>
                                    </div>
                                    <div class="post-card">
                                        {{--  <div class="post-cat">{{ $post->category->name}}</div>  --}}
                                        <a href="post/{{ $post->id }}-{{ $post->slug }}/" class="post-title">{!!$post->title!!}</a>
                                        <div class="post-date">{{ $post->created_at}}</div>
                                        <div class="post-content">
                                            <p>{{ $post->description }} </p>
                                        </div>
                                        <a href="post/{{ $post->id }}-{{ $post->slug }}/" class="post-read">Read More</a>

                                    </div> 
                                </div> 
                            </div>
                    
                        @endforeach   
                    @else
                        <h1><strong>aucun article na encore etait publier</strong></h1>
                    @endif
                </div>
            </div>
            <div class="panel-heading" style="display:flex; justify-content:center;align-items:center;">
                {{$posts->links()}}
            </div>  
        </div>
    </div>
@endsection
{{--  @auth
    // The user is authenticated...
@endauth

@guest
    // The user is not authenticated...
@endguest  --}}