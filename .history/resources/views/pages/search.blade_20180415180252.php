@extends('layouts.app')
@section('title')
    News 
@endsection
@section('content')
    <div class="container-fluid">        
        <div class="row">
            <div class="col-md-12">
                {!! Form::open(['method'=>'GET','url'=>'news','class'=>'navbar-form navbar-left','role'=>'search'])  !!}                       
                    <div class="input-group custom-search-pages">
                        <input type="text" class="form-control" name="search" placeholder="Search..." size="80">
                        <span class="input-group-btn">
                        <button class="btn btn-default-sm" type="submit"><i class="fa fa-search">recherche</button>
                        </span>
                    </div>        
                {!! Form::close() !!}
            </div>
        
        <div id="news">
                <h1>Search</h1>
                    @if(count($posts) > 0)
                        @foreach ($posts as $post)

                            {{-- condition pour afficher la categorie  --}}
                            
                            {{--  {{ $post->category_id = str_replace('1', ' actualite', $post->category_id) }}  --}}

                                {{-- condition pour afficher un article publier  --}}
                                
                                    <div class="col-md-8 col-lg-8">
                                        <ul class="col" data-id="{{$post->id}}">                        
                                                        {{--  choix 1  --}}
                                                <li class="bcolor">
                                                    <a href="post/{{ $post->id }}/">
                                                        <img src="@if( !filter_var($post->image, FILTER_VALIDATE_URL)){{ Voyager::image( $post->image ) }}@else{{ $post->image }}@endif"
                                                                class="img-responsive img-height"
                                                                alt="{{ $post->title }} avatar">
                                                            <h3>{!!$post->title!!}</h3>
                                                            <p class="">{!!$post->excerpt!!}</p>
                                                        </a>
                                                        {{--  <span class="label">{!!$post->body!!}</span>  --}}
                                                        <a href="#news" class="tag">{{ $post->category->name}} </a>
                                                </li>
                                        </ul>
                                    </div>
                    
                        @endforeach   
                    @else
                        <h1><strong>aucun article na encore etait publier</strong></h1>
                    @endif
                </div>
            </div>
            <div class="panel-heading" style="display:flex; justify-content:center;align-items:center;">
                {{$posts->links()}}
            </div>  
        </div>
    </div>
@endsection
{{--  @auth
    // The user is authenticated...
@endauth

@guest
    // The user is not authenticated...
@endguest  --}}