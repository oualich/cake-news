@extends('layouts.app')
@section('title')
    Home 
@endsection
@section('script')
<script defer src="https://use.fontawesome.com/releases/v5.0.8/js/all.js"></script>
@endsection

@section('content')
    <div class="container-fluid">        
        <div class="row">
            <div class="col-md-10 col-md-offset-2">
                <div class="">
                        <div class="panel-body">
                            @if (session('status'))
                                <div class="alert alert-success">
                                    {{ session('status') }}
                                </div>
                            @endif
                        </div>
                </div>
            </div>
<style>
.thumbnail:hover {
background-color: #f1eded  ;
}
.thumbnail{
margin-bottom: 10px;
}
.thumbnail>img {
height: auto;
}
.date-of-post{
color: #0089d8;
}
.news-blog .page-header{
margin-bottom: 13px;
padding: 17px 14px 19px 30px;
margin-top: 12px;
}
.caption h2, h3 {
font-family: inherit;
font-weight: 500;
line-height: 1.1;
color: inherit;
}
.caption p{
display: block;
-webkit-margin-before: 1em;
-webkit-margin-after: 1em;
-webkit-margin-start: 0px;
-webkit-margin-end: 0px;
margin: 0 0 10px;
line-height: 1.42857143;
font-size: 14px;
text-align: justify;
}
</style>
            <div id="news">
                <div class="bord_rubrique">
                    <h1 class="entete_deroule tt5">News World</h1>
                </div>
                @if(count($posts) > 0)
<div class="container-fluid news-blog">
    <div class="row">
                    @foreach ($posts->take(8) as $post)
                        {{-- <p> Posted by <strong> {{ $post->category->name}} </strong> </p> --}}

                        {{-- condition pour afficher la categorie  --}}
                        @if ($post->category->name == "News")
                        {{--  {{ $post->category_id = str_replace('1', ' actualite', $post->category_id) }}  --}}
                               
                                             {{-- condition pour afficher un article publier  --}}                            



       <div class="col-lg-3">

             <a href="#">
                <div class="thumbnail principal-post">
 <img src="@if( !filter_var($post->image, FILTER_VALIDATE_URL)){{ Voyager::image( $post->image ) }}@else{{ $post->image }}@endif" class="img-responsive img-height" alt="{{ $post->title }} avatar">                   <div class="caption">
                    <h2>{!!$post->title!!}</h2>
                      <span class="date-of-post">{{ $post->category->name}}</span>
                   </div>
                   {!!$post->body!!}  <br> 
 {{-- <span class="">{!!$post->body!!}</span>  --}}
                </div>
             </a>
       </div>

                        @else
                        @endif
                    @endforeach  
                       </div>
</div>
                    <div class="col-md-8 col-offset-2">
                        <a class="btn btn-primary btn-sm text-center">  Read more news</a>       
                    </div>
                @else
                    <h1><strong>aucun article na encore etait publier</strong></h1>
                @endif
            </div>
        </div>
        <div class="row">
                    
            <div id="jeux">
                <div class="bord_rubrique">
                    <h1 class="entete_deroule tt5">Science/High-Tech</h1>
                </div>
                @if(count($games) > 0)
                
                    @foreach ($games->take(8) as $post)
                    {{-- condition pour afficher la categorie  --}}
                        @if ($post->category->name == "Science")                        
                        {{--  {{ $post->category_id = str_replace('1', ' actualite', $post->category_id) }}  --}}
                            
                            {{-- condition pour afficher un article publier  --}}
                        
                            
                                <div class="col-md-3 col-lg-3 box-home">
                                    <ul class="col" data-id="{{$post->id}}">                        
                                                    {{--  choix 1  --}}
                                            <li class="bcolor">
                                                <a href="post/{{ $post->id }}/">
                                                    <img src="@if( !filter_var($post->image, FILTER_VALIDATE_URL)){{ Voyager::image( $post->image ) }}@else{{ $post->image }}@endif"
                                                            class="img-responsive img-height"
                                                            alt="{{ $post->title }} avatar">
                                                        <h3>{!!$post->title!!}</h3>
                                                        <p class="">{!!$post->excerpt!!}</p>
                                                    </a>
                                                    {{--  <span class="label">{!!$post->body!!}</span>  --}}
                                                    <a href="#jeux" class="tag">{{ $post->category->name }} </a>
                                            </li>
                                    </ul>
                                </div>
                                        @else
                        @endif

                    @endforeach  
                    <div class="col-md-8 col-offset-2">
                        <a class="btn btn-primary btn-sm text-center"> Read more news</a>       
                    </div> 
                @else
                    <h1><strong>aucun article na encore etait publier</strong></h1>
                @endif
            </div>
        </div>
           
    </div>
    
@endsection
{{--  @auth
    // The user is authenticated...
@endauth

@guest
    // The user is not authenticated...
@endguest  --}}