@extends('layouts.app')
 <style>


 </style> 
 @if (Auth::user())
@section('title')
    dashboard posts
@endsection

@section('script')

<script src="https://cloud.tinymce.com/stable/tinymce.min.js"></script>
<script>tinymce.init({ selector:'textarea' });</script>

@endsection
@section('content')
            <div class="col-md-12">
                <div id="main-content">
                    <div id="main-wrapper">
                        <div id="col-left">
                            <ul>
                                <li class="title-menu">MENU</li>
                                <li id="dashboard" class="active"><i class="fas fa-tachometer-alt fa-lg"></i><a href="http://127.0.0.1:8000/dashboard">Tableau de bord</a></li>
                                <li id="membres"><i class="fas fa-users fa-lg"></i><a href="http://127.0.0.1:8000/communautes"> Membres</a></li>
                                <li id="publications"><i class="far fa-newspaper fa-lg"></i><a href="http://127.0.0.1:8000/dashboard/posts"> Actualités</a></li>
                            </ul>
                            <ul>
                                <li class="title-menu">FONCTIONNALITÉS</li>
                                {{-- <li id="ajoutMembre"><i class="fa fa-plus-circle fa-lg"></i> Ajouter un membre</li> --}}
                                <li id="ajoutArticle" ><i class="fa fa-plus-circle fa-lg"></i><a href="http://127.0.0.1:8000/dashboard/posts/create"> Ajouter une actualité </a></li>
                            </ul>
                            <ul>
                                <li class="title-menu">RÉGLAGES</li>
                                <li id="compte"><i class="fa fa-cog fa-lg"></i><a href="/profile"> Mon compte</a></li>
                                <li id="index"><i class="fa fa-power-off fa-lg"></i><a href="{{ route('logout') }}"  onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();"> Déconnexion</a></li>
                            </ul>
                        </div>
                        {{--  <a class="nav-link btn btn-success btn-add-new" href="post/create"><i class="voyager-plus"></i>Add New</a>  --}}
                            <div class="col-md-9">
 
                                    <div class="col-md-12">
                                    <div id="col-middle">
                                        <h1>Actualités</h1>

                                        <!-- STATISTIQUES -->
                                        <a class="btn btn-medium btn-green btn-radius" href="ajoutArticle.php">Ajouter une Actualité</a>

                                        <div id="col-list">
                                            @if(count($posts) > 0)
                                                <div class="col-md-12">
                                                @foreach ($posts as $post)

                                                    <div class="col-middle-wrapper list">

                                                        <div class="">
                                                            <div class="list-img">
                                                                <img class="img-responsive" src="/storage/{!! $post->image !!}" style="">
                                                            </div>
                                                            <div class="list-title">
                                                                <span>{!!$post->title!!} </span>
                                                                
                                                                <a href="/post/{{$post->id}}" class=" showItem">View</a> /
                                                                <a href="/post/{{$post->id}}/edit" class="edit-btn">Edit</a> /
                                                                {!! Form::open(['action' => ['PostsController@destroy', $post->id], 'methode'=>'POST', 'class' => 'pull-right']) !!}
                                                                    {{ Form::hidden('_method', 'Delete') }} 
                                                                    {{ Form::bsSubmit('Delete', ['class' => 'delete-btn'])}}
                                                                {!! Form::close() !!}
                                                                {{-- <a href="#" class="showItem">Afficher</a> / <a class="edit-btn" href="#" data-id="{!!$post->id!!} ">Modifier</a> /  --}}
                                                                {{-- <a class="delete-btn" href="#" data-id="{!!$post->id!!} ">Supprimer</a> --}}
                                                            </div>
                                                        </div>
                                                        
                                                        <div class="line"></div>
                                                        <div class="list-detail">
                                                            <div class="image-wrapper">
                                                                <img class="img-responsive" src="/storage/{!! $post->image !!}" style="">
                                                            </div><br />
                                                            <div class="detail-wrapper">
                                                                <span>
                                                                    @if($post->status == 'PUBLISHED')
                                                                    <p style="color:darkcyan">{{$post->status}}</p>
                                                                    @elseif($post->status == 'PENDING')
                                                                    <p style="color:tan">{{$post->status}}</p>
                                                                    @endif</span>
                                                                <span></span>
                                                            <span>$post->excerpt </span><br />
                                                                
                                                                    <br /><br /><a class="voir-plus" href="" target="_blank">Voir plus</a>

                                                            </div>
                                                        </div>
                                                    </div>

                                                @endforeach
                                            </div>
                                                <div class="panel-heading" style="display:flex; justify-content:center;align-items:center;">
                                                    {{$posts->links()}}
                                                </div>
                                            @else
                                            <h1><strong>aucun article na encore etait publier</strong></h1>
                                            @endif
                                        </div>
                                    </div>
                                    

                                </div>
                            </div>
                            </div>
                        </div>
                    </div>
                        @section('sidebar')
                            @parent
                            <p>this is appended to the sidebar</p>
                        @endsection  
                        

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@else
        <script>window.location.href = "/login";</script>   
@endif