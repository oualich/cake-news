@extends('layouts.app')
<style> 
    .story-body img {
        width: 73rem;
        height: 36rem;
    }
    .boxmain{
        padding-left: 19.1rem !important;
        width: 91.9rem !important;
    }
    .story-body{
        width: 630px;
        margin:auto;
    }



    @import url(https://fonts.googleapis.com/css?family=Oswald|Roboto:400);
@import url(http://weloveiconfonts.com/api/?family=brandico);

/* brandico */
[class*="brandico-"]:before {
  font-family: "brandico", sans-serif;
  font-size: 1.5em;
  transition: 0.5s color;
}
.brandico-facebook:hover {
  color: #3b5998;
}
.brandico-twitter-bird:hover {
  color: #00acee;
}
.brandico-tumblr:hover {
  color: #34526f;
}
.brandico-linkedin:hover {
  color: #0e76a8;
}

.body-title {
  font-family: "Oswald";
  text-transform: uppercase;
}
.body-title h1 {
  font-size: 2em;
  border-bottom: 0.2em solid;
}
.body-title h1 {
  font-size: 1.3em;
}
#content {
  font-family: "Roboto";
}
#share {
  border-top: 0.2em solid;
}
#share p {
  font-family: "Oswald";
  font-size: 1.4em;
  text-transform: uppercase;
}
#share a {
  text-decoration: none;
  color: #0a0a0a;
}

</style>
@section('script')
<script defer src="https://use.fontawesome.com/releases/v5.0.8/js/all.js"></script>

@endsection
    @section('content')
    <div class="story-body">
        <p> Posted by <strong> {{ $posts->author->name}} </strong> on {!!  $posts->created_at->format('l jS \\of F Y h:i:s A') !!}</p>

      
        {{--  choix 1  --}}
         <img src="@if( !filter_var($posts->image, FILTER_VALIDATE_URL)){{ Voyager::image( $posts->image ) }}@else{{ $posts->image }}@endif"
            class="img-responsive"
             alt="{{ $posts->title }} avatar">

        {{--  choix 2  --}}
        {{--  <img class="img-responsive" src="/storage/{{ $posts->image }}" style="">  --}}
        <hr>
          <div class="body-title">
            <h3>{{$posts->title}} <br></h3>
          </div>
        <p> {!! $posts->body !!}</p>
        <div class="col-md-12">
        <h6 class="text-center">À propos de l auteur</h6>
       
         
        <div class="col-md-2">
          <img src="@if( !filter_var($posts->author->avatar , FILTER_VALIDATE_URL)){{ Voyager::image( $posts->author->avatar  ) }}@else{{ $posts->author->avatar  }}@endif"
                                    class="profile-img"
                                     alt="{{ $posts->author->avatar }} avatar">
      </div>
       <div class="col-md-10">
      <p>{{ $posts->author->bio}}</p> 
            <p> author : <strong>{{ $posts->author->name}} </strong></p>  
        </div>
        </div>
        <div id="disqus_thread"></div>
    </div>
    
  
  <script type="text/javascript">
    var disqus_shortname = 'blog-crvqzgoyjh';
    @if (isset($slug))
      var disqus_identifier = 'blog-{{ $slug }}';
    @endif

    (function() {
      var dsq = document.createElement('script');
      dsq.type = 'text/javascript';
      dsq.async = true;
      dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
      (document.getElementsByTagName('head')[0] ||
        document.getElementsByTagName('body')[0]).appendChild(dsq);
    })();
  </script>
  <noscript>
    Please enable JavaScript to view the
    <a href="http://disqus.com/?ref_noscript">comments powered by Disqus.</a>
  </noscript>
  <a href="http://disqus.com" class="dsq-brlink">
    comments powered by <span class="logo-disqus">Disqus</span>
  </a>

@endsection