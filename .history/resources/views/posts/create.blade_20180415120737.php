@extends('layouts.app')
<style>
.panel{
padding:20px;
}
</style>
@if (Auth::user())
@section('title')
    Create posts
@endsection

@section('script')
<script defer src="https://use.fontawesome.com/releases/v5.0.8/js/all.js"></script>
<script src="https://cloud.tinymce.com/stable/tinymce.min.js"></script>
<script>tinymce.init({ selector:'textarea' });</script>

@endsection

@section('content')
    <h1> Add Post        </h1>
    {!! Form::open(['action' => 'PostsController@store', 'methode'=>'POST', 'enctype' => 'multipart/form-data']) !!}
       <div class="col-md-8">
        <div class="panel">           
            <div class="panel-heading">
                {{-- {{ Form::bsText('title') }} --}}
                {{Form::label('title', 'Post Title')}}
                <br>
                {{Form::text('title', '', ['class' =>'form-control','placeholder'=>'enter name'])}}
                </div>
            </div>
        
            <div class="panel">
                <div class="panel-heading">

                    {{ Form::bsTextArea('body') }}
            </div>
        </div>
        <div class="panel">
            <div class="panel-heading">
                {{Form::label('description', 'Description mini')}}
                <br>
                {{Form::text('description', '', ['class' =>'form-control','placeholder'=>'enter name'])}}  
                {{-- {{ Form::bsText('excerpt') }} --}}
            </div>
        </div>
        {{ Form::hidden('author_id',Auth::user()->id) }}
        </div>
        <div class="col-md-4">
        <div class="panel panel panel-bordered panel-warning">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="icon wb-clipboard"></i> Post Details</h3>
                <div class="panel-actions">
                    <a class="panel-action voyager-angle-down" data-toggle="panel-collapse" aria-hidden="true"></a>
                </div>
            </div> <br>
            <div class="form-group">
            
            {{Form::label('slug', 'Meta')}}
            <br>
            {{Form::text('slug', '', ['class' =>'form-control','placeholder'=>'enter meta'])}}
            </div> <br>
            <div class="form-group">
            {!! Form::label('name', 'Categories *') !!}<br>

            {{ Form::select('category_id', ['1' => 'Actualité', '2' => 'Jeux'])  }}
            </div>
            <br>
            <div class="form-group">
            {!! Form::label('name', 'Status *') !!} <br>

            {{ Form::select('status', ['PUBLISHED' => 'PUBLISHED', 'DRAFT' => 'DRAFT', 'PENDING' => 'PENDING']) }}
            </div>
        </div>
        <div class="panel panel-bordered panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="icon wb-image"></i> Post Image</h3>
                <div class="panel-actions">
                    <a class="panel-action voyager-angle-down" data-toggle="panel-collapse" aria-hidden="true"></a>
                </div>
            </div>
            <div class="panel-body">
                {{ Form::file('image') }}
            </div>
        </div>       
        
                {{ Form::bsSubmit('Submit', ['class' => 'btn btn-primary'])}}

    </div>

    {!! Form::close() !!}
    @endsection
@else
        <script>window.location.href = "/login";</script>    </script>
@endif