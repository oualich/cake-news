                    <div id="col-middle">
                        <h1>Actualités</h1>

                        <!-- STATISTIQUES -->
                        <a class="btn btn-medium btn-green btn-radius" href="ajoutArticle.php">Ajouter une Actualité</a>


                        <div id="col-list">
                            @if(count($posts) > 0)
                                <div class="col-md-4">
                                @foreach ($posts as $post)

                                    <div class="col-middle-wrapper list">

                                        <div class="">
                                            <div class="list-img">
                                                <img class="img-responsive" src="/storage/{!! $post->image !!}" style="">
                                            </div>
                                            <div class="list-title">
                                                <span><?php echo $row["titre"]; ?></span>
                                                <a href="#" class="showItem">Afficher</a> / <a class="edit-btn" href="#" data-id="<?php echo $row["id"]; ?>">Modifier</a> / <a class="delete-btn" href="#" data-id="<?php echo $row["id"]; ?>">Supprimer</a>
                                            </div>
                                        </div>
                                        <img src="images/Dropdown.PNG" alt="drop down" />
                                        <div class="line"></div>
                                        <div class="list-detail <?php echo wd_remove_accents(strtolower(str_replace(" ", "-", $row['type']))); ?>">
                                            <?php if(!empty($row["image"])){ ?>
                                            <div class="image-wrapper">
                                                <img src="../<?php echo $row["image"]; ?>" alt="image" />
                                            </div><br />
                                            <?php } ?>
                                            <div class="detail-wrapper">
                                                <span><?php echo $row["type"]; ?></span>
                                                <span></span>
                                                
                                                }
                                                ?>
                                                <?php echo $row["texte"]; ?><br />
                                                <?php if(!empty($row["lien"])){ ?>
                                                    <br /><br /><a class="voir-plus" href="<?php echo $row["lien"]; ?>" target="_blank">Voir plus</a>
                                                <?php
                                                }
                                                ?>
                                            </div>
                                        </div>
                                    </div>

                                @endforeach
                            </div>
                            @else
                            <h1><strong>aucun article na encore etait publier</strong></h1>
                            @endif
                        </div>
                    </div>