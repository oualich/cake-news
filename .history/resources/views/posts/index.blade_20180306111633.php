@extends('layouts.app')
<style>
.panel.widget h4 {
    color: #fff;
    font-weight: 300;
    margin-top: 20px;
    font-size: 20px;
}
.panel.widget p {
    margin: 30px 0 0;
    font-size: 20px;
    font-size: 14px;
    color: #DDD;
    display: block;
    max-height: 65px;
}
.panel.widget.bgimage {
    background-size: cover;
    background-position: center center;
    position: relative;
}
.panel.widget.center {
    text-align: center;
}
</style>
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    @section('content')
                    
                    <a class="nav-link btn btn-success btn-add-new" href="post/create"><i class="voyager-plus"></i>Add New</a>
                    <div class="col-md-6"></div>                        
                        @if(count($posts) > 0)
                            <div class="col-md-4">
                                @foreach ($posts as $post)
                                
                                    <div class="well">
                                un
                                        <h6><a href="post/{{$post->id}}">{!!$post->title!!} </a></h6>
                                        <img class="img-responsive" src="/storage/{!! $post->image !!}" style="">
                                        <span>{!!$post->excerpt!!}</span>

                                        {{--  <span>{!!$post->body!!}</span>  --}}

                                            <div class="pull-right">
                                                <a href="/post/{{$post->id}}" class="btn btn-sm btn-warning view">View</a>
                                                <a href="/post/{{$post->id}}/edit" class="btn btn-sm btn-primary edit">Edit</a>
                                                {!! Form::open(['action' => ['PostsController@destroy', $post->id], 'methode'=>'POST', 'class' => 'pull-right']) !!}
                                                    {{ Form::hidden('_method', 'Delete') }} 
                                                    {{ Form::bsSubmit('Delete', ['class' => 'btn btn-sm btn-danger delete'])}}
                                                {!! Form::close() !!}
                                            </div>  
                                            <div>
                                                    <label>status</label>
                                                    @if($post->status == 'PUBLISHED')
                                                    <p style="color:darkcyan">{{$post->status}}</p>
                                                    @elseif($post->status == 'PENDING')
                                                    <p style="color:tan">{{$post->status}}</p>
                                                    @endif
                                                    {{--  <p>{{$post->status}}</p>  --}}
                                                </div>    
                                    </div>
                                
                            

                                @endforeach
                            </div>
                            @else
                            <h1><strong>aucun article na encore etait publier</strong></h1>
                            @endif
                            

                        @endsection
                    
                    {{--  @section('sidebar')
                        @parent
                        <p>this is appended to the sidebar</p>
                    @endsection  --}}
                    

                </div>
            </div>
        </div>
    </div>
</div>

@endsection
