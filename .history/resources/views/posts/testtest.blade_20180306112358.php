                    <div id="col-middle">
                        <h1>Actualités</h1>

                        <!-- STATISTIQUES -->
                        <a class="btn btn-medium btn-green btn-radius" href="ajoutArticle.php">Ajouter une Actualité</a>


                        <div id="col-list">
                            @if(count($posts) > 0)
                                <div class="col-md-4">
                                @foreach ($posts as $post)

                                    <div class="col-middle-wrapper list">

                                        <div class="<?php echo wd_remove_accents(strtolower(str_replace(" ", "-", $row['type']))); ?>">
                                            <div class="list-img">
                                                <img class="img-responsive" src="/storage/{!! $post->image !!}" style="">
                                            </div>
                                            <div class="list-title">
                                                <span>{!!$post->title!!} </span>
                                                <a href="#" class="showItem">Afficher</a> / <a class="edit-btn" href="#" data-id="{!!$post->id!!} ">Modifier</a> / <a class="delete-btn" href="#" data-id="{!!$post->id!!} ">Supprimer</a>
                                            </div>
                                        </div>
                                        <img src="images/Dropdown.PNG" alt="drop down" />
                                        <div class="line"></div>
                                        <div class="list-detail <?php echo wd_remove_accents(strtolower(str_replace(" ", "-", $row['type']))); ?>">
                                            <div class="image-wrapper">
                                                <img class="img-responsive" src="/storage/{!! $post->image !!}" style="">
                                            </div><br />
                                            <div class="detail-wrapper">
                                                <span>
                                                    @if($post->status == 'PUBLISHED')
                                                    <p style="color:darkcyan">{{$post->status}}</p>
                                                    @elseif($post->status == 'PENDING')
                                                    <p style="color:tan">{{$post->status}}</p>
                                                    @endif</span>
                                                <span></span>
                                                
                                                }
                                                ?>
                                               <span>$post->status </span><br />
                                                
                                                    <br /><br /><a class="voir-plus" href="" target="_blank">Voir plus</a>

                                            </div>
                                        </div>
                                    </div>

                                @endforeach
                            </div>
                            @else
                            <h1><strong>aucun article na encore etait publier</strong></h1>
                            @endif
                        </div>
                    </div>