@extends('layouts.app')
 <style>

    .select-style {
        padding: 0;
        margin: 0;
        border: 1px solid #ccc;
        width: 190px;
        border-radius: 3px;
        overflow: hidden;
        background-color: #fff;

        background: #fff url("../images/arrow_sans_down.png") no-repeat 90% 50%;
    }

    .select-style select {
        padding: 5px 8px;
        width: 130%;
        border: none;
        box-shadow: none;
        background-color: transparent;
        background-image: none;
        -webkit-appearance: none;
        -moz-appearance: none;
                appearance: none;
    }

    .select-style select:focus {
        outline: none;
    }

    div.list {
        min-height: auto !important;
        border-radius: 5px !important;
        overflow: hidden;
        padding: 0 !important;
        position: relative;
        margin-top: 20px !important;
    }

    div.list > div:first-child {
        position: relative;
        top: 0;
        left: 0;
        min-height: auto;
        background-color: white;
        padding: 15px 15px;
        display: block;
    }

    div.news {
        border-left: 4px solid #89C4F4;
    }


    div.nouveau-membre {
        border-left: 4px solid #F25236;
    }

    div.avantage-club {
        border-left: 4px solid #BE90D4;
    }

    div.resultat-competition {
        border-left: 4px solid #FFB94E;
        text-align: left;
    }

    div.alerte {
        border-left: 4px solid #87D37C;
        text-align: left;
    }

    .list-img {
        height: 55px;
        width: 55px;
        border-radius: 50%;
        overflow: hidden;
        display: inline-block;
        vertical-align: middle;
        margin-right: 25px;
    }

    .list-img img {
        width: 100%;
        transform-origin: center center;
        position: relative;
        top: 50%;
        transform: translateY(-50%);
    }

    .list-title {
        display: inline-block;
        vertical-align: middle;
        color: #7c8ca5;
        font-size: 13px;
    }

    .list-title a {
        color: #7c8ca5;
        text-decoration: none;
    }

    .list-title span {
        display: block;
        color: #354052;
        font-weight: 600;
        font-size: 15px;
    }

    div.list > div + img {
        position: absolute;
        top: 50%;
        right: 30px;
        transition: .3s all ease;
        -moz-transition: .3s all ease;
        -o-transition: .3s all ease;
        -webkit-transition: .3s all ease;
        -ms-transition: .3s all ease;
    }

    .rotate-arrow {
        transform: rotate(180deg);
        transition: .3s all ease;
        -moz-transition: .3s all ease;
        -o-transition: .3s all ease;
        -webkit-transition: .3s all ease;
        -ms-transition: .3s all ease;
    }

    /* --- detail --- */
    .list-detail {
        display: none;
        background-color: white;
        font-size: 13px;
        padding: 20px;
        text-align: center;
    }

    .line {
        width: 100%;
        height: 1px;
        background-color: #EFF3F6;
        display: none;
    }

    .list-detail div.image-wrapper {
        display: inline-block;
        padding: 5px;
        border: 1px solid #bfc6d1;
        border-radius: 3px;
    }

    .list-detail img {
        max-width: 200px;
        display: inline-block;
        border-radius: 3px;
    }

    .list-detail div.detail-wrapper {
        display: inline-block;
        text-align: left;
    }

    .list-detail div.detail-wrapper span {
        display: block;
        color: #354052;
        font-weight: 600;
        font-size: 15px;
        margin: 15px 0;
    }

    /* --- button --- */
    a.btn,.btn {
    margin-right: 1em; /* remove this while use*/
    margin-bottom: 1em; /* remove this while use*/
        display: inline-block;
        outline: none;
        *zoom: 1;
        text-align: center;
        text-decoration: none;
        font-family: inherit;
        font-weight: 300;
        letter-spacing: 1px;
        vertical-align: middle;
        border: 1px solid;
        transition: all 0.2s ease;
        box-sizing: border-box;
        text-shadow: 0 1px 0 rgba(0,0,0,0.01);
        position: relative;
        left: 100%;
        transform: translateX(-100%);
    }

    .btn-radius {
    border-radius: 3px;
    }

    .btn-medium {
        font-size: 0.9375em;
        padding: 0.5375em 1.375em;
    }

    .btn-green {
        color: #3CB371;
        border-color: #3CB371;
    }
    .btn-green:hover {
    background: #3CB371;
    color: #fff;
    border-color: #3CB371;		
    }

    /* -- delete button --- */
    .delete-btn {
        color: #e74c3c !important;
    }

    /* --- Voir plus --- */
    .voir-plus {
        padding: 8px 15px;
        background-color: #40B474;
        color: white !important;
        border-radius: 3px;
    }
 </style> 
 @if (Auth::user())

@section('content')
            <div class="col-md-12">
                <div id="main-content">
                    <div id="main-wrapper">
                        <div id="col-left">
                            <ul>
                                <li class="title-menu">MENU</li>
                                <li id="dashboard" class="active"><i class="fas fa-tachometer-alt fa-lg"></i><a href="http://127.0.0.1:8000/dashboard">Tableau de bord</a></li>
                                <li id="membres"><i class="fas fa-users fa-lg"></i><a href="http://127.0.0.1:8000/communautes"> Membres</a></li>
                                <li id="publications"><i class="far fa-newspaper fa-lg"></i><a href="http://127.0.0.1:8000/dashboard/posts"> Actualités</a></li>
                            </ul>
                            <ul>
                                <li class="title-menu">FONCTIONNALITÉS</li>
                                {{-- <li id="ajoutMembre"><i class="fa fa-plus-circle fa-lg"></i> Ajouter un membre</li> --}}
                                <li id="ajoutArticle" ><i class="fa fa-plus-circle fa-lg"></i><a href="http://127.0.0.1:8000/dashboard/posts/create"> Ajouter une actualité </a></li>
                            </ul>
                            <ul>
                                <li class="title-menu">RÉGLAGES</li>
                                <li id="compte"><i class="fa fa-cog fa-lg"></i><a href="/profile"> Mon compte</a></li>
                                <li id="index"><i class="fa fa-power-off fa-lg"></i><a href="{{ route('logout') }}"  onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();"> Déconnexion</a></li>
                            </ul>
                        </div>
                        {{--  <a class="nav-link btn btn-success btn-add-new" href="post/create"><i class="voyager-plus"></i>Add New</a>  --}}
                            <div class="">
                                <div class="panel panel-default">
                                <div class="panel-heading">Dashboard</div>

                                <div class="panel-body">
                                    @if (session('status'))
                                        <div class="alert alert-success">
                                            {{ session('status') }}
                                        </div>
                                    @endif
                                    <div class="col-md-12">
                                    <div id="col-middle">
                                        <h1>Actualités</h1>

                                        <!-- STATISTIQUES -->
                                        <a class="btn btn-medium btn-green btn-radius" href="ajoutArticle.php">Ajouter une Actualité</a>

                                        <div id="col-list">
                                            @if(count($posts) > 0)
                                                <div class="col-md-4">
                                                @foreach ($posts as $post)

                                                    <div class="col-middle-wrapper list">

                                                        <div class="">
                                                            <div class="list-img">
                                                                <img class="img-responsive" src="/storage/{!! $post->image !!}" style="">
                                                            </div>
                                                            <div class="list-title">
                                                                <span>{!!$post->title!!} </span>
                                                                
                                                                <a href="/post/{{$post->id}}" class="btn btn-sm btn-warning view showItem">View</a>
                                                                <a href="/post/{{$post->id}}/edit" class="btn btn-sm btn-primary edit edit-btn">Edit</a>
                                                                {!! Form::open(['action' => ['PostsController@destroy', $post->id], 'methode'=>'POST', 'class' => 'pull-right']) !!}
                                                                    {{ Form::hidden('_method', 'Delete') }} 
                                                                    {{ Form::bsSubmit('Delete', ['class' => 'delete-btn'])}}
                                                                {!! Form::close() !!}
                                                                {{-- <a href="#" class="showItem">Afficher</a> / <a class="edit-btn" href="#" data-id="{!!$post->id!!} ">Modifier</a> /  --}}
                                                                {{-- <a class="delete-btn" href="#" data-id="{!!$post->id!!} ">Supprimer</a> --}}
                                                            </div>
                                                        </div>
                                                        
                                                        <div class="line"></div>
                                                        <div class="list-detail">
                                                            <div class="image-wrapper">
                                                                <img class="img-responsive" src="/storage/{!! $post->image !!}" style="">
                                                            </div><br />
                                                            <div class="detail-wrapper">
                                                                <span>
                                                                    @if($post->status == 'PUBLISHED')
                                                                    <p style="color:darkcyan">{{$post->status}}</p>
                                                                    @elseif($post->status == 'PENDING')
                                                                    <p style="color:tan">{{$post->status}}</p>
                                                                    @endif</span>
                                                                <span></span>
                                                            <span>$post->excerpt </span><br />
                                                                
                                                                    <br /><br /><a class="voir-plus" href="" target="_blank">Voir plus</a>

                                                            </div>
                                                        </div>
                                                    </div>

                                                @endforeach
                                            </div>
                                            @else
                                            <h1><strong>aucun article na encore etait publier</strong></h1>
                                            @endif
                                        </div>
                                    </div>
                                    

                                </div>
                            </div>
                            </div>
                        </div>
                    </div>
                        @section('sidebar')
                            @parent
                            <p>this is appended to the sidebar</p>
                        @endsection  
                        

                    </div>
                </div>
            </div>
        </div>
    </div>





                        <div class="col-md-8 col-md-offset-2">
                            <div class="panel panel-default">
                                <div class="panel-heading">Dashboard</div>

                                <div class="panel-body">
                                    @if (session('status'))
                                        <div class="alert alert-success">
                                            {{ session('status') }}
                                        </div>
                                    @endif
                                    <div class="col-md-12">
                                    <div id="col-middle">
                                        <h1>Actualités</h1>

                                        <!-- STATISTIQUES -->
                                        <a class="btn btn-medium btn-green btn-radius" href="ajoutArticle.php">Ajouter une Actualité</a>

                                        <div id="col-list">
                                            @if(count($posts) > 0)
                                                <div class="col-md-4">
                                                @foreach ($posts as $post)

                                                    <div class="col-middle-wrapper list">

                                                        <div class="">
                                                            <div class="list-img">
                                                                <img class="img-responsive" src="/storage/{!! $post->image !!}" style="">
                                                            </div>
                                                            <div class="list-title">
                                                                <span>{!!$post->title!!} </span>
                                                                
                                                                <a href="/post/{{$post->id}}" class="btn btn-sm btn-warning view showItem">View</a>
                                                                <a href="/post/{{$post->id}}/edit" class="btn btn-sm btn-primary edit edit-btn">Edit</a>
                                                                {!! Form::open(['action' => ['PostsController@destroy', $post->id], 'methode'=>'POST', 'class' => 'pull-right']) !!}
                                                                    {{ Form::hidden('_method', 'Delete') }} 
                                                                    {{ Form::bsSubmit('Delete', ['class' => 'delete-btn'])}}
                                                                {!! Form::close() !!}
                                                                {{-- <a href="#" class="showItem">Afficher</a> / <a class="edit-btn" href="#" data-id="{!!$post->id!!} ">Modifier</a> /  --}}
                                                                {{-- <a class="delete-btn" href="#" data-id="{!!$post->id!!} ">Supprimer</a> --}}
                                                            </div>
                                                        </div>
                                                        
                                                        <div class="line"></div>
                                                        <div class="list-detail">
                                                            <div class="image-wrapper">
                                                                <img class="img-responsive" src="/storage/{!! $post->image !!}" style="">
                                                            </div><br />
                                                            <div class="detail-wrapper">
                                                                <span>
                                                                    @if($post->status == 'PUBLISHED')
                                                                    <p style="color:darkcyan">{{$post->status}}</p>
                                                                    @elseif($post->status == 'PENDING')
                                                                    <p style="color:tan">{{$post->status}}</p>
                                                                    @endif</span>
                                                                <span></span>
                                                            <span>$post->excerpt </span><br />
                                                                
                                                                    <br /><br /><a class="voir-plus" href="" target="_blank">Voir plus</a>

                                                            </div>
                                                        </div>
                                                    </div>

                                                @endforeach
                                            </div>
                                            @else
                                            <h1><strong>aucun article na encore etait publier</strong></h1>
                                            @endif
                                        </div>
                                    </div>
                                    

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>  

@endsection
@else
        <script>window.location.href = "/login";</script>   
@endif