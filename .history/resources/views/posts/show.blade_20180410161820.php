@extends('layouts.app')
<style> 
    .story-body img {
        width: 73rem;
        height: 36rem;
    }
    .boxmain{
        padding-left: 19.1rem !important;
        width: 91.9rem !important;
    }
    .story-body{
        width: 630px;
        margin:auto;
    }
</style>
@section('script')
<script defer src="https://use.fontawesome.com/releases/v5.0.8/js/all.js"></script>

@endsection
    @section('content')
    <div class="story-body">
        <p> Posted by <strong> {{ $posts->author->name}} </strong> on {!!  $posts->created_at->format('l jS \\of F Y h:i:s A') !!}</p>

      
        {{--  choix 1  --}}
         <img src="@if( !filter_var($posts->image, FILTER_VALIDATE_URL)){{ Voyager::image( $posts->image ) }}@else{{ $posts->image }}@endif"
            class="img-responsive"
             alt="{{ $posts->title }} avatar">

        {{--  choix 2  --}}
        {{--  <img class="img-responsive" src="/storage/{{ $posts->image }}" style="">  --}}
        <hr>
         <h3>{{$posts->title}} <br></h3>
        <p> {!! $posts->body !!}</p>
        {{-- <div id="disqus_thread"></div> --}}
        <div id="disqus_thread"></div>
    </div>
    
  
  <script type="text/javascript">
    var disqus_shortname = 'blog-crvqzgoyjh';
    @if (isset($slug))
      var disqus_identifier = 'blog-{{ $slug }}';
    @endif

    (function() {
      var dsq = document.createElement('script');
      dsq.type = 'text/javascript';
      dsq.async = true;
      dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
      (document.getElementsByTagName('head')[0] ||
        document.getElementsByTagName('body')[0]).appendChild(dsq);
    })();
  </script>
  <noscript>
    Please enable JavaScript to view the
    <a href="http://disqus.com/?ref_noscript">comments powered by Disqus.</a>
  </noscript>
  <a href="http://disqus.com" class="dsq-brlink">
    comments powered by <span class="logo-disqus">Disqus</span>
  </a>

@endsection