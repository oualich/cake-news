@extends('layouts.app')
@if (Auth::user())
    @section('title')
        Edit posts
    @endsection

    @section('script')
        <script defer src="https://use.fontawesome.com/releases/v5.0.8/js/all.js"></script>
        <script src="https://cloud.tinymce.com/stable/tinymce.min.js"></script>
        <script>tinymce.init({ selector:'textarea' });</script>

    @endsection

    @section('content')
        <h1> Edit article {{$posts->id}}</h1>
        <div class="col-md-8">
            
            {!! Form::open(['action' => ['PostsController@update', $posts->id], 'methode'=>'POST']) !!}
                <div class="panel">           
                    <div class="panel-heading">
                        {{ Form::bsText('title', $posts->title) }}
                    </div>
                </div>
                {{ Form::bsTextArea('body', $posts->body) }}
                {{ Form::bsText('description', $posts->description) }}
                {{ Form::bsText('slug', $posts->slug) }}

                {{ Form::hidden('_method', 'PUT') }}
                <a class="btn btn-default" href="/post/{{$posts->id}}">Go Back</a>
   
              {{ Form::bsSubmit('Submit', ['class' => 'btn btn-primary'])}}
            {!! Form::close() !!}
            <!-- ### CONTENT ### -->
        </div>
        <div class="col-md-4">
            <div class="panel panel-bordered panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="icon wb-image"></i> Post Image</h3>
                        <div class="panel-actions">
                            <a class="panel-action voyager-angle-down" data-toggle="panel-collapse" aria-hidden="true"></a>
                        </div>
                    </div>
                    <div class="panel-body">
                        <img src="@if( !filter_var($posts->image, FILTER_VALIDATE_URL)){{ Voyager::image( $posts->image ) }}@else{{ $posts->image }}@endif"
                                    class="img-responsive"
                                    alt="{{ $posts->title }} avatar">
                        <input type="file" name="image">
                    </div>
            </div>
        </div>

    @endsection
@else
        <script>window.location.href = "/login";</script>   
@endif