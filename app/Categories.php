<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Traits\HasRelationships;
use TCG\Voyager\Traits\Translatable;
use User;
Use Post;


class Categories extends Model
{
    //
    use Translatable,
    HasRelationships;
    protected $table = 'categories';

    protected $fillable = [
    'name',
    ];
    public function categoriesid()
    {
         return $this->belongsTo(Post::class);
        // return $this->belongsTo(Voyager::modelClass('Categories'));

    }
}
