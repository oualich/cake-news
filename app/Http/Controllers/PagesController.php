<?php

namespace App\Http\Controllers;

// use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Input;

use App\Post;
use App\User;
use App\Categories;
use View;
use Auth;

use Request;
use Illuminate\Support\Facades\Storage;


class PagesController extends Controller
{
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function getHome(){
       
        $request = Post::where('status' , 'PUBLISHED')
        ->orderBy('posts.created_at' , 'DESC')
        ->paginate(5, ['*'], 'home-continu');
                
        $posts = Post::select('posts.*')->search()
        ->join('categories', 'categories.id', '=', 'posts.category_id')
        ->orderBy('posts.created_at' , 'DESC')
        ->where('status' , 'PUBLISHED')
        ->paginate(8);

        $allposts = Post::orderBy('posts.category_id' , 'DESC')->where('status' , 'PUBLISHED')->get();

        return view('home', ['posts'=>$posts,'request'=>$request,'allposts'=>$allposts]);
    
    }


    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
     public function getnews(){

        $posts = Post::select('posts.*')->search()
        ->join('categories', 'categories.id', '=', 'posts.category_id')
        ->where('categories.name', '=','news')
        ->orderBy('posts.created_at' , 'DESC')
        ->where('status' , 'PUBLISHED')
        ->paginate(8);


        return view('pages.news', compact('posts'))->with('posts', $posts);

    
    }

        /**
         * Display a listing of the resource.
         *
         * @return \Illuminate\Http\Response
         */
        
        public function getscience(){


            $posts = Post::select('posts.*')->search()
            ->join('categories', 'categories.id', '=', 'posts.category_id')
            ->where('categories.name', '=','science')
            ->orderBy('posts.created_at' , 'DESC')
            ->where('status' , 'PUBLISHED')
            ->paginate(8);
    
            return view('pages.science', compact('posts'))->with('posts', $posts);

        
        }
                /**
         * Display a listing of the resource.
         *
         * @return \Illuminate\Http\Response
         */
        
        public function gethightech(){

            $posts = Post::select('posts.*')->search()
            ->join('categories', 'categories.id', '=', 'posts.category_id')
            ->where('categories.name', '=','high-tech')
            ->orderBy('posts.created_at' , 'DESC')
            ->where('status' , 'PUBLISHED')
            ->paginate(8);
             // SELECT * FROM posts 
             // INNER JOIN categories 
             // ON posts.category_id = categories.id 
             // WHERE categories.name = 'High-tech' AND posts.status = 'PUBLISHED' 
             // ORDER BY posts.created_at = 'DESC' ;

            return view('pages.high-tech', compact('posts'))->with('posts', $posts);

        }

                        /**
         * Display a listing of the resource.
         *
         * @return \Illuminate\Http\Response
         */
        
        public function getpolitique(){

            $posts = Post::select('posts.*')->search()
            ->join('categories', 'categories.id', '=', 'posts.category_id')
            ->where('categories.name', '=','politique')
            ->orderBy('posts.created_at' , 'DESC')
            ->where('status' , 'PUBLISHED')
            ->paginate(8);


            return view('pages.politique', compact('posts'))->with('posts', $posts);

        
        }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
     public function getAbout(){
    
        return view('pages.about');
    
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
     public function getContact(){
    
        return view('pages.contact');
    
    }
   /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function getSearch(){

        $posts = Post::search()->where('status' , 'PUBLISHED')->paginate(8);

        $request = Post::where('status' , 'PUBLISHED')
        ->paginate(5, ['*'], 'home-continu');

        return view('search.search', ['posts'=>$posts,'request'=>$request]);

    
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     public function getDashboard(){
    
        $posts = Post::all();
        return view('pages.dashboard')
            ->with('posts', $posts)
            ->with('posts_count', Post::all()->count())
            ->with('category_count', Categories::all()->count())

            ->with('users_count', User::all()->count());
    
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     public function getCommunautes(){
    
        $users = User::all();
        return view('pages.communautes')->with('users', $users);
    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function getDashboardpost()
    {
        //



        $posts = Post::orderBy('created_at', 'desc')
        
        ->select('posts.*')
        ->where('posts.author_id', '=' ,Auth::user()->id)
        ->paginate(6);

        return view('admin.posts')->with('posts', $posts);
    }

        /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function getDashboardmembres()
    {
        //
        $users = User::orderBy('created_at', 'desc')->paginate(5);
        return view('admin.membres')->with('users', $users);

        // $users = User::orderBy('created_at', 'desc')->paginate(5);
        // return view('admin.membres')->with('users', $users);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
     public function destroy($id)
    {
        //
        $posts = Post::find($id);
        $posts->delete();
        return redirect('/')->with('succes','posts delete');
    }

}

