<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;

use App\User;
use Illuminate\Support\Facades\Storage;


class UserController extends Controller
{
    public function index()
    {
        //
        $posts = User::all();
        return view('users.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('users.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, [
            'text' => 'required'
        ]);

        // create User
        $posts = new User;
        $posts->text = $request->input('text');
        $posts->body = $request->input('body');
        $posts->due = $request->input('due');

        $posts->save();

        return redirect('/')->with('success', 'user create'); 

    }
 
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $posts = User::find($id);
        return view('users.show')->with('posts', $posts);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $posts = User::find($id)->get();
        return view('users.edit')->with('posts', $posts);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // update User

        $user = User::find($id);
    
        $user->firstname = $request->input('firstname');
        $user->email = $request->input('email');
        $user->name = $request->input('name');
        $user->bio = $request->input('bio');
        if ($request->hasFile('avatar')) {
            $file = $request->file('avatar');
            $fileName = $file->getClientOriginalName();
            $destinationPath = base_path() . '/storage/app/public/users/';
            $move = $request->file('avatar')->move($destinationPath, $fileName);
            $namefile = 'users/' .$fileName;
            $user->avatar =$namefile;
        };
        $user->save();


        return redirect('/')->with('success', 'user update'); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $posts = Posts::find($id);
        $posts->delete();
        return redirect('/')->with('succes','Todo delete');
    }
}
