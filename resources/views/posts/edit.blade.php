@extends('layouts.app')
@if (Auth::user())
    @section('title')
        Edit posts
    @endsection

    @section('script')
        <script defer src="https://use.fontawesome.com/releases/v5.0.8/js/all.js"></script>
        <script src="https://cloud.tinymce.com/stable/tinymce.min.js"></script>
        <script>tinymce.init({ selector:'textarea' });</script>

    @endsection

    @section('content')
        <h1> Edit article</h1>
        {!! Form::open(['action' => ['PostsController@update', $posts->id], 'methode'=>'POST','enctype'=>'multipart/form-data']) !!}

        <div class="col-md-8">
            
                <div class="panel">           
                    <div class="panel-heading">
                        {{Form::label('title', "Titre de l'article")}} <br>
                        {{ Form::text('title', $posts->title,['class' =>'form-control panel-title','placeholder'=>'Titre']) }}
                    </div>
                </div>
                <div class="panel">
                    <div class="panel-heading">
                        {{Form::label('body', "Contenu de l'article")}}
                        {{Form::textarea('body', $posts->body) }}
                    </div>
                </div>
                <div class="panel">
                    <div class="panel-heading">                
                        {{ Form::bsText('excerpt', $posts->excerpt) }}
                    </div>
                </div>
            </div>


   
            <!-- ### CONTENT ### -->
       
        <div class="col-md-4 voyager">
            <div class="panel panel panel-bordered panel-warning">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="icon wb-clipboard"></i>Détails de l'article</h3>
                    <div class="panel-actions">
                        <a class="panel-action voyager-angle-down" data-toggle="panel-collapse" aria-hidden="true"></a>
                    </div>  
                </div>                <br> 
                <div class="panel-body">
                    <div class="form-group">
                    {{Form::label('slug', 'Slug URL')}} 
                    <br>
                    {{ Form::text('slug', $posts->slug,['class' =>'form-control panel-title','placeholder'=>' slug']) }}
                </div>
                <div class="form-group">
                        {!! Form::label('name', "Catégorie de l'article") !!}<br>
    
                        {{-- {{ Form::select('category_id',[$posts->category_id])  }} --}}
                        
                        <select name="category_id" id="category_id" type="text">
                                  <option value="{{$posts->category->id}}">
                                    {{$posts->category->name}}
                                  </option>

                        </div>
            </div>
            <div class="panel panel-bordered panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="icon wb-image"></i> Post Image</h3>
                        <div class="panel-actions">
                            <a class="panel-action voyager-angle-down" data-toggle="panel-collapse" aria-hidden="true"></a>
                        </div>
                    </div>

                    {{ Form::hidden('_method', 'PUT') }}
    
                    <div class="panel-body">
                        <img src="@if( !filter_var($posts->image, FILTER_VALIDATE_URL)){{ Voyager::image( $posts->image ) }}@else{{ $posts->image }}@endif"
                                    class="img-responsive"
                                    alt="{{ $posts->title }} avatar">
                    </div>
                    {!! Form::file('image', $posts->file) !!}

            </div>
        </div>
        {{ Form::bsSubmit('Submit', ['class' => 'btn btn-primary'])}}

        {!! Form::close() !!}
        <a class="btn btn-default" href="/post/{{$posts->id}}">Go Back</a>

    @endsection
@else
{{ Redirect::to('/login') }}

        {{-- <script>window.location.href = "/login";</script>    --}}
@endif