@extends('layouts.app')

@section('script')
<script defer src="https://use.fontawesome.com/releases/v5.0.8/js/all.js"></script>

@endsection
@section('content')
<div class="container-fluid">
  <div class="row">
        <div class="col-md-8">

          <div class="story-body">

            <div id="content">
              <p> Posted by <strong> {{ $posts->author->name}} </strong> on {!!  $posts->created_at->format('l jS \\of F Y y:m:d ') !!}</p>
            </div>

            <img src="@if( !filter_var($posts->image, FILTER_VALIDATE_URL)){{ Voyager::image( $posts->image ) }}@else{{ $posts->image }}@endif"
                  class="img-responsive"
                  alt="{{ $posts->title }} avatar">
            
                  <hr>

            <div class="body-title">
              <h3>{{$posts->title}} <br></h3>
            </div>

            <div id="content">
              <p> {!! $posts->body !!}</p>
            </div>

            <div class="col-md-12"style="border: 1px solid;">
                <h6 class="text-center">À propos de l auteur</h6>
                <div class="col-md-2" >
                  <img src="@if( !filter_var($posts->author->avatar , FILTER_VALIDATE_URL)){{ Voyager::image( $posts->author->avatar  ) }}@else{{ $posts->author->avatar  }}@endif"
                                            class="profile-img"
                                            alt="{{ $posts->author->avatar }} avatar" style="width: 50px;
                                            height: 50px; border-radius: 50% 50%;">
                </div>
                <div class="col-md-10">
                      <p>{!! $posts->author->bio!!}</p> 
                      <p> author : <strong>{{ $posts->author->name}} </strong></p>  
                </div>
            </div>

            <div id="disqus_thread"></div>

          </div>

          <script type="text/javascript">
              var disqus_shortname = 'blog-crvqzgoyjh';
              @if (isset($slug))
                var disqus_identifier = 'blog-{{ $slug }}';
              @endif

              (function() {
                var dsq = document.createElement('script');
                dsq.type = 'text/javascript';
                dsq.async = true;
                dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
                (document.getElementsByTagName('head')[0] ||
                  document.getElementsByTagName('body')[0]).appendChild(dsq);
              })();
          </script>

          </div>
        <div class="col-md-4">

            <div class="pub">

                <div class="single_leftbar wow fadeInDown animated" style="visibility: visible; animation-name: fadeInDown;">
                    <h2><span>Popular Post</span></h2>
                    <div class="singleleft_inner">
                        <ul class="catg3_snav ppost_nav wow fadeInDown animated" style="visibility: visible; animation-name: fadeInDown;">
                            @foreach ($pub as $item)
                                
                            
                            <li id="divEffect" class="shadow2 wow fadeInRight animated" style="visibility: visible; animation-name: fadeInRight;">
                                <div class="media">
                                    <a  href="/post/{{ $item->id }}-{{ $item->slug }}/" class="media-left">
                                        <img src="https://www.sshakil.com/public/uploads/article_image/re_sized/1509116298.jpg" class="img-responsive img-thumbnail img-center" alt="Make bootstrap carousel dynamic in Laravel" style="width: auto; height: auto;">
                                    </a>

                                    <div class="media-body">
                                        <a  href="/post/{{ $item->id }}-{{ $item->slug }}/" class="catg_title hvr-underline-from-center">
                                           {{ $item->title}}
                                        </a>
                                    </div>
                                </div>
                            </li>


                            @endforeach
                            {{$pub->links()}}

                        </ul>
                    </div>
                </div>

            </div>
        </div>
      </div>

      <style>
        .single_leftbar {

float: left;
display: inline;
width: 100%;
margin-bottom: 25px;

}
.singleleft_inner {

background-color: #fff;
display: inline;
float: left;
padding: 15px;
width: 100%;

}
#divEffect {

border-radius: 5px !important;
background-color: #ffffff;
padding: 10px 10px;
box-shadow: 0 1px 4px rgba(0, 0, 0, 0.3), 0 0 20px rgba(0, 0, 0, 0.2) inset;
margin-bottom: 10px;
-moz-box-shadow: 0 4px 5px 0 rgba(0,0,0,.14), 0 1px 10px 0 rgba(0,0,0,.12), 0 2px 4px -1px rgba(0,0,0,.2);
-webkit-box-shadow: 0 4px 5px 0 rgba(0,0,0,.14), 0 1px 10px 0 rgba(0,0,0,.12), 0 2px 4px -1px rgba(0,0,0,.2);
box-shadow: 1px 1px 3px 2px #DCDCDC;
box-shadow: 0 4px 5px 0 rgba(0,0,0,.14), 0 1px 10px 0 rgba(0,0,0,.12), 0 2px 4px -1px rgba(0,0,0,.2);

}
.media:first-child {

margin-top: 0;

}
.media {

margin-top: 15px;

}
.media-left {

width: 120px;
height: auto;

}
.media-left, .media-right, .media-body {

display: table-cell;
vertical-align: top;

}
.hvr-underline-from-center {

padding-bottom: 5px !important;

}
.hvr-underline-from-center {

display: inline-block;
vertical-align: middle;
-webkit-transform: perspective(1px) translateZ(0);
transform: perspective(1px) translateZ(0);
box-shadow: 0 0 1px transparent;
position: relative;
overflow: hidden;

}
.catg3_snav .media-left > img {

    width: 100%;
    height: 100%;

}
        </style>
</div>
@endsection