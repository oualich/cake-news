@extends('layouts.app')
<style>
.panel{
padding:20px;
}
</style>
@if (Auth::user())
@section('title')
    Create posts
@endsection

@section('script')
<script defer src="https://use.fontawesome.com/releases/v5.0.8/js/all.js"></script>
<script src="https://cloud.tinymce.com/stable/tinymce.min.js"></script>

@endsection

@section('content')
{{-- <link rel="stylesheet" href="http://127.0.0.1:8000/vendor/tcg/voyager/assets/css/app.css"> --}}
    <div class="col-md-12">
        <div class="navbar-header">
            <button class="hamburger btn-link">
            <span class="hamburger-inner"></span>
            </button>
            <ol class="breadcrumb hidden-xs">
            <li class="active">
            <a href="http://127.0.0.1:8000/dashboard"><i class="voyager-boat"></i> Tableau de bord</a>
            </li>
            <li class="active"><a href="http://127.0.0.1:8000/dashboard/posts">Posts</a>
            </li>
            <li>Ajout</li>
            </ol>
        </div>
    </div>
    <h1> Ajouter Post   </h1>

    <div class="col-md-12">
    {!! Form::open(['action' => 'PostsController@store', 'methode'=>'POST', 'enctype' => 'multipart/form-data']) !!}
        <div class="row">
            <div class="col-md-8">
                <div class="panel">           
                    <div class="panel-heading">
                        {{-- {{ Form::bsText('title') }} --}}
                        {{Form::label('title', "Titre de l'article")}} <br>
                        <span class="panel-desc"> Le titre de votre article</span>
                        <br>
                        {{Form::text('title', '', ['class' =>'form-control panel-title','placeholder'=>'Titre'])}}
                        </div>
                    </div>
                
                    <div class="panel">
                        <div class="panel-heading">
                                {{Form::label('body', "Contenu de l'article")}}

                            {{ Form::textarea('body') }}
                    </div>
                </div>
                <div class="panel">
                    <div class="panel-heading">
                        {{Form::label('excerpt', 'Extrait')}}
                        <br>
                        {{Form::text('excerpt', '', ['class' =>'form-control panel-title','placeholder'=>''])}}  
                        {{-- {{ Form::bsText('excerpt') }} --}}
                    </div>
                </div>
                {{ Form::hidden('author_id',Auth::user()->id) }}
            </div>
            <div class="col-md-4 voyager">
                {{-- test --}}
                {{-- test --}}
                <div class="panel panel panel-bordered panel-warning">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="icon wb-clipboard"></i>Détails de l'article</h3>
                        <div class="panel-actions">
                            <a class="panel-action voyager-angle-down" data-toggle="panel-collapse" aria-hidden="true"></a>
                        </div>
                    </div> 
                    <br>
                    <div class="panel-body">
                        <div class="form-group">
                        
                        {{Form::label('slug', 'Slug URL')}}
                        <br>
                        {{Form::text('slug', '', ['class' =>'form-control panel-title','placeholder'=>' slug'])}}
                        </div> <br>
                        <div class="form-group">
                            {!! Form::label('name', "Catégorie de l'article") !!}<br>

                            {{-- {{ Form::select('category_id',['1' => 'Actualité', '2' => 'Science', '3' => 'High-Tech', '4'=>'Politique','5'=>'Sport','6'=>'Musique'])  }} --}}
                        {{ Form::select('category_id', $categories) }}

                        </div>
                        <br>
                        <div class="form-group">
                        {!! Form::label('name', "Statut de l'article") !!} <br>

                        {{ Form::select('status', ['PUBLISHED' => 'PUBLISHED', 'DRAFT' => 'DRAFT', 'PENDING' => 'PENDING']) }}
                        </div>
                    </div>
                </div>
                
                <div class="panel panel-bordered panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="icon wb-image"></i> Image de l'article                    </h3>
                        <div class="panel-actions">
                            <a class="panel-action voyager-angle-down" data-toggle="panel-collapse" aria-hidden="true"></a>
                        </div>
                    </div>
                    <div class="panel-body">
                        {{ Form::file('image') }}
                    </div>
                </div>    
                    {{ Form::bsSubmit('Submit', ['class' => 'btn btn-primary'])}}
            </div>
        </div>
    {!! Form::close() !!}
</div>

    <script type="text/javascript" async="" src="https://s3.amazonaws.com/laravelvoyager/voyager.js"></script>
    <script type="text/javascript" src="http://127.0.0.1:8000/vendor/tcg/voyager/assets/js/app.js"></script>
    <script>tinymce.init({ selector:'textarea' });</script>

    @endsection

@else
        <script>window.location.href = "/login";</script>    </script>
@endif