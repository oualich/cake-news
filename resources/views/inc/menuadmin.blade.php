<ul>
    <li class="title-menu">MENU</li>
    <li id="dashboard" class="active"><i class="fas fa-tachometer-alt fa-lg"></i><a href="{{ route('dashboard') }}">Tableau de bord</a></li>
    <li id="membres"><i class="fas fa-users fa-lg"></i><a href="{{ route('dashboard.member') }}"> Membre</a></li>
    <li id="publications"><i class="far fa-newspaper fa-lg"></i><a href="{{ route('dashboard.posts') }}"> Actualités</a></li>
</ul>
<ul>
    <li class="title-menu">FONCTIONNALITÉS</li>
    {{-- <li id="ajoutMembre"><i class="fa fa-plus-circle fa-lg"></i> Ajouter un membre</li> --}}
    <li id="ajoutArticle" ><i class="fa fa-plus-circle fa-lg"></i><a href="{{ route('dashboard.posts.create') }}"> Ajouter une actualité </a></li>
</ul>
<ul>
    <li class="title-menu">RÉGLAGES</li>
    <li id="compte"><i class="fa fa-cog fa-lg"></i><a href="{{ route('account.index') }}"> Mon compte</a></li>
    <li id="index"><i class="fa fa-power-off fa-lg"></i><a href="{{ route('logout') }}"  onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();"> Déconnexion</a></li>
</ul>