@extends('layouts.app')
@section('title')
    News 
@endsection
@section('content')
    <div class="container-fluid">        
        <div class="row">
            <div class="col-md-12">
                {!! Form::open(['method'=>'GET','url'=>'politique','class'=>'navbar-form navbar-left','role'=>'search'])  !!}                       
                    <div class="input-group custom-search-form">
                            <input  type="search" name="search" placeholder="Search">
                            <span class="input-group-btn">
                            </span>
                    </div>        
                {!! Form::close() !!}
            </div>
        

        </div>
        <section id="news" style="background: rgb(193, 193, 193);">
                    <div class="bord_rubrique">
                        <h1 class="entete_deroule tt5">News politique</h1>
                    </div>
                    <div class="container">
                            <div class="d-flex">
                      
                              @if(count($posts) > 0)
                                      
                                  @foreach ($posts as $post)
                                        <div class="tflex-content">
                                          <div class="div-article-home">
                                              <div class="post-image" style="background:url('@if( !filter_var($post->image, FILTER_VALIDATE_URL)){{ Voyager::image( $post->image ) }}@else{{ $post->image }}@endif')">
                                                  <div class="post-author">{{ $post->category->name}}</div>
                                              </div>
                                              <div class="post-card">
                                                  <a href="post/{{ $post->id }}-{{ $post->slug }}/" class="post-title">{!!$post->title!!}</a>
                                                  <div class="post-date">{{ $post->created_at}}</div>
                                                  <div class="post-content"style="">
                                                      <p>{!! $post->excerpt !!} </p>
                                                  </div>
                                                  <a href="post/{{ $post->id }}-{{ $post->slug }}/" class="post-read">Read More</a>
                                              </div> 
                                          </div> 
                                        </div>
                                  @endforeach  
                      
                              @else
                                  <h1><strong>Aucun article n'a été publié veuiller revenir plus tard</strong></h1>
                              @endif
                      
                            </div>      
                          </div>    
            </section>
            <div class="panel-heading" style="display:flex; justify-content:center;align-items:center;">
                {{$posts->links()}}
            </div>  
        </div>
    </div>
@endsection
{{--  @auth
    // The user is authenticated...
@endauth

@guest
    // The user is not authenticated...
@endguest  --}}