@extends('layouts.app')
@if (Auth::user())
@section('title')
    dashboard 
@endsection
@section('script')
<script defer src="https://use.fontawesome.com/releases/v5.0.8/js/all.js"></script>
@endsection
@section('content')
            <div class="col-md-12">

                <div class="col-md-4">
                    <ul>
                        <li class="title-menu">MENU</li>
                        <li id="dashboard" class="active"><i class="fas fa-tachometer-alt fa-lg"></i><a href="{{ route('dashboard') }}">Tableau de bord</a></li>
                        <li id="membres"><i class="fas fa-users fa-lg"></i><a href="{{ route('dashboard.member') }}"> Member</a></li>
                        <li id="publications"><i class="far fa-newspaper fa-lg"></i><a href="{{ route('dashboard.posts') }}"> Actualités</a></li>
                    </ul>
                    <ul>
                        <li class="title-menu">FONCTIONNALITÉS</li>
                        {{-- <li id="ajoutMembre"><i class="fa fa-plus-circle fa-lg"></i> Ajouter un membre</li> --}}
                        <li id="ajoutArticle" ><i class="fa fa-plus-circle fa-lg"></i><a href="{{ route('dashboard.posts.create') }}"> Ajouter une actualité </a></li>
                    </ul>
                    <ul>
                        <li class="title-menu">RÉGLAGES</li>
                        <li id="compte"><i class="fa fa-cog fa-lg"></i><a href="{{ route('account.index') }}"> Mon compte</a></li>
                        <li id="index"><i class="fa fa-power-off fa-lg"></i><a href="{{ route('logout') }}"  onclick="event.preventDefault();
                                            document.getElementById('logout-form').submit();"> Déconnexion</a></li>
                    </ul>
                </div>
                {{--  <a class="nav-link btn btn-success btn-add-new" href="post/create"><i class="voyager-plus"></i>Add New</a>  --}}
                <div class="col-md-8">
                    <div class="col-md-6">
                        <div class="panel widget center bgimage" style="height: 200px;
                        margin-bottom:0;overflow:hidden;background-image:url('http://127.0.0.1:8000/vendor/tcg/voyager/assets/images/widget-backgrounds/02.jpg');">
                            <div class="dimmer"></div>
                            <div class="panel-content">
                                <i class="voyager-news"></i><h4>{{ $posts_count }} Posts</h4>
                                <p>la base de donnée indique que il y a {{ $posts_count }} Posts au total. <br> Click sur button pour afficher tout les posts.</p>
                                <a href="{{ route('dashboard.posts') }}" class="btn btn-primary">View my posts</a>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="panel widget center bgimage" style="height: 200px;margin-bottom:0;overflow:hidden;background-image:url('http://127.0.0.1:8000/vendor/tcg/voyager/assets/images/widget-backgrounds/02.jpg');">
                            <div class="dimmer"></div>
                            <div class="panel-content">
                                <i class="voyager-news"></i><h4>{{ $users_count }} users</h4>
                                <p>Il  y a actuellement {{ $users_count }} users sur la database. <br> Click sur button pour afficher les utilisateur.</p>
                                <a href="{{ route('dashboard.member') }}" class="btn btn-primary">View all users</a>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-8 col-md-offset-2">
                        <div class="panel widget center bgimage" style="height: 200px;margin-bottom:0;overflow:hidden;background-image:url('http://127.0.0.1:8000/vendor/tcg/voyager/assets/images/widget-backgrounds/02.jpg');">
                            <div class="dimmer"></div>
                            <div class="panel-content">
                                <i class="voyager-news"></i><h4>{{ $category_count }} category</h4>
                                <p>Le site contient au total {{ $category_count }} category.</p>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
                        
@endsection
@else
        <script>window.location.href = "/login";</script>   
@endif