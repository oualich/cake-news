@extends('layouts.app')
@section('title')
    Home 
@endsection
@section('script')
<script defer src="https://use.fontawesome.com/releases/v5.0.8/js/all.js"></script>

@endsection

@section('content')
<style>

/* .d-flex :nth-of-type(1) {
height: 473.5px;
} */

/* // test */
/* .d-flex :nth-child(5n) {
margin-top: -2.2rem;
}
.d-flex :nth-child(6n) {
  margin-top: -2.2rem;
} */

</style>
 <div class="container-fluid">
    
    <div class="row">
        <div class="col-md-12">
            <div class="panel-body">
                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif
            </div>
        </div>
    </div>
    
    <div class="bord_rubrique">
      <h1 class="entete_deroule tt5">News World</h1>
    </div>
    
    <div class="container">
      <div class="d-flex">

        @if(count($posts) > 1)
                
            @foreach ($posts as $post)
                  <div class="tflex-content">
                    <div class="div-article-home">
                        <div class="post-image" style="background:url('@if( !filter_var($post->image, FILTER_VALIDATE_URL)){{ Voyager::image( $post->image ) }}@else{{ $post->image }}@endif')">
                            <div class="post-author">{{ $post->category->name}}</div>
                        </div>
                        <div class="post-card">
                            <a href="post/{{ $post->id }}-{{ $post->slug }}/" class="post-title">{!!$post->title!!}</a>
                            <div class="post-date">{{ $post->created_at}}</div>
                            <div class="post-content"style="">
                                <p>{!! $post->excerpt !!} </p>
                            </div>
                            <a href="post/{{ $post->id }}-{{ $post->slug }}/" class="post-read">Read More</a>
                        </div> 
                    </div> 
                  </div>
            @endforeach  

        @else
            <h1><strong>Aucun article n'a été publié veuiller revenir plus tard</strong></h1>
        @endif

      </div>      
    </div>    


  </div>
<script>
        $('.slider').each(function() {
        var $this = $(this);
        var $group = $this.find('.slide_group');
        var $slides = $this.find('.slide');
        var bulletArray = [];
        var currentIndex = 0;
        var timeout;
        
        function move(newIndex) {
          var animateLeft, slideLeft;
          
          advance();
          
          if ($group.is(':animated') || currentIndex === newIndex) {
            return;
          }
          
          bulletArray[currentIndex].removeClass('active');
          bulletArray[newIndex].addClass('active');
          
          if (newIndex > currentIndex) {
            slideLeft = '100%';
            animateLeft = '-100%';
          } else {
            slideLeft = '-100%';
            animateLeft = '100%';
          }
          
          $slides.eq(newIndex).css({
            display: 'block',
            left: slideLeft
          });
          $group.animate({
            left: animateLeft
          }, function() {
            $slides.eq(currentIndex).css({
              display: 'none'
            });
            $slides.eq(newIndex).css({
              left: 0
            });
            $group.css({
              left: 0
            });
            currentIndex = newIndex;
          });
        }
        
        function advance() {
          clearTimeout(timeout);
          timeout = setTimeout(function() {
            if (currentIndex < ($slides.length - 1)) {
              move(currentIndex + 1);
            } else {
              move(0);
            }
          }, 4000);
        }
        
        $('.next_btn').on('click', function() {
          if (currentIndex < ($slides.length - 1)) {
            move(currentIndex + 1);
          } else {
            move(0);
          }
        });
        
        $('.previous_btn').on('click', function() {
          if (currentIndex !== 0) {
            move(currentIndex - 1);
          } else {
            move(3);
          }
        });
        
        $.each($slides, function(index) {
          var $button = $('<a class="slide_btn">&bull;</a>');
          
          if (index === currentIndex) {
            $button.addClass('active');
          }
          $button.on('click', function() {
            move(index);
          }).appendTo('.slide_buttons');
          bulletArray.push($button);
        });
        
        advance();
        });
</script>
      
@endsection
