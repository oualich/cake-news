<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('title')</title>
    @yield('css_page')
    @yield('script')
     <script defer src="https://use.fontawesome.com/releases/v5.0.8/js/all.js"></script> 
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="/css/app.css">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <script src="{{ asset('js/app.css') }}"></script>
    <style>

    </style>
</head>
<body>

            @include('inc.navbar')     
            @if(Request::is('/')) 
                <section>
                    <div class="container-fluid">
                        <div class="row">
                            <section class="slider-home">
                                <div class="container-fluid">
                                    
                                    <div class="container">

                                    <div class="col-xs|sm|md|lg|xl-1-12 masthead">
                                        <ul class="masthead-menu">
                                            <li class="date">{{date('l \\, j  F Y ')}}</li>
                                            <li class="todays-paper"><a href="http://www.nytimes.com/pages/todayspaper/index.html" data-collection="todays-paper"><i class="icon sprite-icon"></i>Today’s Paper</a></li>
                                            <li class="video"><a href="https://www.nytimes.com/video" data-collection="video"><i class="icon sprite-icon"></i>Video</a></li>
                                        </ul>
                                    </div>

                                    
                                        <div class="row">
                                    
                                            {{-- <div class="col-md-8 col-lg-8">
                                                @include('inc.showcase')
                                            </div>
                                            <div class="col-md-2 col-lg-2 col-md-offset-2">
                                                @include('inc.sidebare')
                                            </div> --}}
                                            <div class="col-md-8 col-lg-8">
                                                @include('inc.showcase')
                                            </div>
                                            <div class="col-md-3 col-lg-3 col-md-offset-1" style="    padding-top: 60px;">
                                                @include('inc.sidebare')
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                            @include('inc.message')
                        </div>
                    </div>
                </section>
            @endif  

    <section class="container-fluid"style="background: #FFFFFF; padding-top:60px;padding-bottom: 60px;">
            @yield('content')
    </section>
    @if(Request::is('/'))
<footer id="footer">

	<div class="container">

			<div class="footer--menus row">

				<aside class="footer--menu col col-12 col-md-4 col-lg-3">
					<h4 class="footer--menu--title text-uppercase is-turquoise">Get Started</h4>
					<div class="menu-get-started-container"><ul id="menu-get-started" class="list-unstyled"><li id="menu-item-34413" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-34413"><a href="https://www.nfb.ca/about/">About this site</a></li>
<li id="menu-item-34414" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-34414"><a href="https://www.nfb.ca/newsletters">Subscribe to our newsletter</a></li>
<li id="menu-item-34415" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-34415"><a href="https://www.nfb.ca/member/register/">Create your free NFB.ca account</a></li>
<li id="menu-item-34416" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-34416"><a href="https://www.nfb.ca/apps/">NFB on TVs and mobile devices</a></li>
</ul></div>				</aside>

				<aside class="footer--menu col col-12 col-md-4 col-lg-3">
					<h4 class="footer--menu--title text-uppercase is-red">Resources</h4>
					<div class="menu-ressources-container"><ul id="menu-ressources" class="list-unstyled"><li id="menu-item-34417" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-34417"><a href="http://help.nfb.ca/">Help Centre</a></li>
<li id="menu-item-34418" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-34418"><a href="http://help.nfb.ca/contact-the-nfb/">Contact us</a></li>
<li id="menu-item-34419" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-34419"><a href="http://onf-nfb.gc.ca/en/produce-with-the-nfb/french-program/nfb-french-program-directing-co-producing/">Produce with the NFB</a></li>
<li id="menu-item-34420" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-34420"><a href="http://mediaspace.nfb.ca/">Media Space</a></li>
</ul></div>				</aside>

				<aside class="footer--menu col col-12 col-md-4 col-lg-3">
					<h4 class="footer--menu--title text-uppercase is-pink">More</h4>
					<div class="menu-more-container"><ul id="menu-more" class="list-unstyled"><li id="menu-item-34409" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-34409"><a href="https://www.nfb.ca/distribution/">NFB Distribution</a></li>
<li id="menu-item-34410" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-34410"><a href="https://www.nfb.ca/education/">NFB Education</a></li>
<li id="menu-item-34411" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-34411"><a href="http://www.nfb.ca/archives">NFB Archives</a></li>
<li id="menu-item-34412" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-34412"><a href="http://www.nfb.ca/dvd-store/">DVD Store</a></li>
</ul></div>				</aside>

				<aside class="footer--menu col col-md-12 col-lg-3">
										<h4 class="footer--menu--title text-uppercase is-purple">Connect with us</h4>
					<ul class="list-unstyled social-links">
						<li><a href="https://www.facebook.com/nfb.ca" title="Facebook"><span class="footer--menu--item-icon icon-nfb-facebook"></span><span class="d-none d-lg-inline-block">Facebook</span></a></li>						<li><a href="https://twitter.com/thenfb/" title="Twitter"><span class="footer--menu--item-icon icon-nfb-twitter"></span><span class="d-none d-lg-inline-block">Twitter</span></a></li>						<li><a href="https://vimeo.com/thenfb" title="Vimeo"><span class="footer--menu--item-icon icon-nfb-vimeo"></span><span class="d-none d-lg-inline-block">Vimeo</span></a></li>						<li><a href="http://www.youtube.com/nfb" title="Youtube"><span class="footer--menu--item-icon icon-nfb-youtube"></span><span class="d-none d-lg-inline-block">Youtube</span></a></li>						<li><a href="https://www.instagram.com/onf_nfb/" title="Instagram"><span class="footer--menu--item-icon icon-nfb-instagram"></span><span class="d-none d-lg-inline-block">Instagram</span></a></li>					</ul>
				</aside>

			</div>

			<div class="footer--utils row align-items-center">
												<div class="col col-md-auto">
					<a href="https://blogue.onf.ca" class="btn btn-outline-primary btn-sm text-uppercase">Français</a>
				</div>
				
				<div class="w-100 d-block d-md-none"></div>

				<div class="col col-md-auto">
					<a href="https://www.canada.ca" class="canada mx-auto text-hide" target="_blank">Canada</a>
				</div>

				<div class="w-100 d-block d-md-none"></div>

				<div class="col">
					© 2018 <a href="https://www.nfb.ca/">National Film Board of Canada</a> <span class="sep">|</span> <a href="http://onf-nfb.gc.ca/">Institutional Website</a> <span class="sep">|</span> <a href="http://help.nfb.ca/important-notices/">Notices</a> <span class="sep">|</span> <a href="http://onf-nfb.gc.ca/en/jobs/">Jobs</a>				</div>
			</div>

	</div>

</footer>
    @endif  

    <script src="{{ asset('js/app.css') }}"></script>

</body>
</html>