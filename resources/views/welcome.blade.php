@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    @section('content')
                        <h1>Home</h1>
                        <p>Quare talis improborum consensio non modo excusatione amicitiae tegenda non est sed potius supplicio omni vindicanda est, ut ne quis concessum putet amicum vel bellum patriae inferentem sequi; quod quidem, ut res ire coepit, haud scio an aliquando futurum sit. Mihi autem non minori curae est, qualis res publica post mortem meam futura, quam qualis hodie sit.
                        </p>
                        @section('content')
                            <h1>todos</h1>
                            
                            @if(count($posts) > 0)
                            @foreach ($posts as $post)
                            <div class="col-md-3">
                                <div class="well">
                                    <h3><a href="todo/{{$todo->id}}">{{$post->title}} </a><span class="label label-danger">{{$post->body}}</span></h3>
                                </div>
                            </div>
                            @endforeach
                        @endif
                        @endsection
                    @endsection
                    
                    @section('sidebar')
                        @parent
                        <p>this is appended to the sidebar</p>
                    @endsection
                    

                </div>
            </div>
        </div>
    </div>
</div>

@endsection
