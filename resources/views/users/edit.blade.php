@extends('layouts.app')
@if (Auth::user())

@section('script')

<script src="https://cloud.tinymce.com/stable/tinymce.min.js"></script>
<script>tinymce.init({ selector:'textarea' });</script>

@endsection
@section('content')    
        <div class="col-md-3">
            <h3>aperçu</h3>
            <div style="text-align:center;">

                <div class="panel"> 
                    <div class="panel-heading">
                            <img src="@if( !filter_var(Auth::user()->avatar, FILTER_VALIDATE_URL)){{ Voyager::image( Auth::user()->avatar ) }}@else{{ Auth::user()->avatar }}@endif"
                            style="width:150px; height:150px; clear:both; display:block; padding:2px; border:1px solid #ddd; margin-bottom:10px;     margin: 0 auto;  border-radius: 50% 50%;
                            "
                                alt="{{ Auth::user()->avatar }} avatar">
                        <h4>{{ ucwords(Auth::user()->name) }}</h4>
                
                        <p>{{ Auth::user()->firstname }}</p>
                    
                        <div class="user-email text-muted">{{ ucwords(Auth::user()->email) }}</div>
                            <br>
                            <p>{!! Auth::user()->bio !!}</p>
                            <a href="http://127.0.0.1:8000/dashboard/account" class="btn btn-primary">back</a>
                            <br>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-9 ">
        {!! Form::open(['action' => ['UserController@update', Auth::user()->id], 'methode'=>'POST','enctype'=>'multipart/form-data']) !!}
            <div class="panel"> 
            
                    <div class="panel-heading">                        
                        <img src="@if( !filter_var(Auth::user()->avatar, FILTER_VALIDATE_URL)){{ Voyager::image( Auth::user()->avatar ) }}@else{{ Auth::user()->avatar }}@endif"
                            style="width:200px; height:200px; clear:both; display:block; padding:2px; border:1px solid #ddd; margin-bottom:10px;     margin: 0 auto;     border-radius: 50% 50%;
                            "
                                alt="{{ Auth::user()->avatar }} avatar">
                        {{ Form::file('avatar') }}
                    </div>
                </div>
                <div class="panel"> 
                <div class="panel-heading">
                        {{ Form::bsText('name', Auth::user()->name) }}
                    </div>
                </div>
                <div class="panel"> 
                    <div class="panel-heading">
                        {{ Form::bsText('firstname', Auth::user()->firstname) }}
                    </div>
                </div>
                <div class="panel"> 
                    <div class="panel-heading">
                        {{ Form::bsText('email', Auth::user()->email) }}
                    </div>
                </div>
            
                <div class="panel">
                    <div class="panel-heading">
                        {{ Form::bsTextArea('bio', Auth::user()->bio) }}
                    </div>
                </div>

                        {{ Form::hidden('_method', 'PUT') }}

                <div class="panel">
                    <div class="panel-heading">
                        {{ Form::bsSubmit('Enregister', ['class' => 'btn btn-primary'])}}
                    </div>
                </div>
            </div>
        {!! Form::close() !!}
        </div>
@endsection
@else
        <script>window.location.href = "/login";</script>   
@endif