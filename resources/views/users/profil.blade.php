@extends('layouts.app')
@if (Auth::user())
    @section('title')
        dashboard posts
    @endsection

    @section('css_page')
        <style>
            .select-style {
            padding: 0;
            margin: 0;
            border: 1px solid #ccc;
            width: 190px;
            border-radius: 3px;
            overflow: hidden;
            background-color: #fff;

            background: #fff url("../images/arrow_sans_down.png") no-repeat 90% 50%;
            }

            .select-style select {
            padding: 5px 8px;
            width: 130%;
            border: none;
            box-shadow: none;
            background-color: transparent;
            background-image: none;
            -webkit-appearance: none;
            -moz-appearance: none;
            // appearance: none;
            }

            .select-style select:focus {
            outline: none;
            }

            div.list {
            min-height: auto !important;
            border-radius: 5px !important;
            overflow: hidden;
            padding: 0 !important;
            position: relative;
            margin-top: 20px !important;
            }

            div.list > div:first-child {
            position: relative;
            top: 0;
            left: 0;
            min-height: auto;
            background-color: white;
            padding: 15px 15px;
            display: block;
            }

            div.news {
            border-left: 4px solid #89C4F4;
            }


            div.nouveau-membre {
            border-left: 4px solid #F25236;
            }

            div.avantage-club {
            border-left: 4px solid #BE90D4;
            }

            div.resultat-competition {
            border-left: 4px solid #FFB94E;
            text-align: left;
            }

            div.alerte {
            border-left: 4px solid #87D37C;
            text-align: left;
            }

            .list-img {
            height: 55px;
            width: 55px;
            border-radius: 50%;
            overflow: hidden;
            display: inline-block;
            vertical-align: middle;
            margin-right: 25px;
            }

            .list-img img {
            width: 100%;
            transform-origin: center center;
            position: relative;
            top: 50%;
            transform: translateY(-50%);
            }

            .list-title {
            display: inline-block;
            vertical-align: middle;
            color: #7c8ca5;
            font-size: 13px;
            }

            .list-title a {
            color: #7c8ca5;
            text-decoration: none;
            }

            .list-title span {
            display: block;
            color: #354052;
            font-weight: 600;
            font-size: 15px;
            }

            div.list > div + img {
            position: absolute;
            top: 50%;
            right: 30px;
            transition: .3s all ease;
            -moz-transition: .3s all ease;
            -o-transition: .3s all ease;
            -webkit-transition: .3s all ease;
            -ms-transition: .3s all ease;
            }

            .rotate-arrow {
            transform: rotate(180deg);
            transition: .3s all ease;
            -moz-transition: .3s all ease;
            -o-transition: .3s all ease;
            -webkit-transition: .3s all ease;
            -ms-transition: .3s all ease;
            }

            /* --- detail --- */
            .list-detail {
            display: none;
            background-color: white;
            font-size: 13px;
            padding: 20px;
            text-align: center;
            }

            .line {
            width: 100%;
            height: 1px;
            background-color: #EFF3F6;
            display: none;
            }

            .list-detail div.image-wrapper {
            display: inline-block;
            padding: 5px;
            border: 1px solid #bfc6d1;
            border-radius: 3px;
            }

            .list-detail img {
            max-width: 200px;
            display: inline-block;
            border-radius: 3px;
            }

            .list-detail div.detail-wrapper {
            display: inline-block;
            text-align: left;
            }

            .list-detail div.detail-wrapper span {
            display: block;
            color: #354052;
            font-weight: 600;
            font-size: 15px;
            margin: 15px 0;
            }

            /* --- button --- */
            a.btn,.btn {
            margin-right: 1em; /* remove this while use*/
            margin-bottom: 1em; /* remove this while use*/
            display: inline-block;
            outline: none;
            *zoom: 1;
            text-align: center;
            text-decoration: none;
            font-family: inherit;
            font-weight: 300;
            letter-spacing: 1px;
            vertical-align: middle;
            border: 1px solid;
            transition: all 0.2s ease;
            box-sizing: border-box;
            text-shadow: 0 1px 0 rgba(0,0,0,0.01);
            position: relative;
            left: 100%;
            transform: translateX(-100%);
            }

            .btn-radius {
            border-radius: 3px;
            }

            .btn-medium {
            font-size: 0.9375em;
            padding: 0.5375em 1.375em;
            }

            .btn-green {
            color: #3CB371;
            border-color: #3CB371;
            }
            .btn-green:hover {
            background: #3CB371;
            color: #fff;
            border-color: #3CB371;		
            }

            /* -- delete button --- */
            .delete-btn {
            color: #e74c3c !important;
            }

            /* --- Voir plus --- */
            .voir-plus {
            padding: 8px 15px;
            background-color: #40B474;
            color: white !important;
            border-radius: 3px;
            }
        </style> 
    @endsection

    @section('script')
    <script defer src="https://use.fontawesome.com/releases/v5.0.8/js/all.js"></script>

    @endsection
    @section('content')
        <div class="col-md-12">
            <div id="main-content">
                <div id="main-wrapper">
                    <div id="col-left">
                        @include('inc.menuadmin')
                    </div>
                    {{--  <a class="nav-link btn btn-success btn-add-new" href="post/create"><i class="voyager-plus"></i>Add New</a>  --}}
                    <div class="col-md-9">

                                <div class="col-md-12">
                                    <div id="col-middle">
                                        <h1>Membres</h1>

                                        <!-- STATISTIQUES -->

                                        <div id="col-list">
                                            <div class="col-md-10">
                                                    <div style="text-align:center;">
                                                        <div class="panel"> 
                                                            <div class="panel-heading">
                                                                    <img src="@if( !filter_var(Auth::user()->avatar, FILTER_VALIDATE_URL)){{ Voyager::image( Auth::user()->avatar ) }}@else{{ Auth::user()->avatar }}@endif"
                                                                    style="width:150px; height:150px; clear:both; display:block; padding:2px; border:1px solid #ddd; margin-bottom:10px;     margin: 0 auto;    border-radius: 50% 50%;
                                                                    "
                                                                        alt="{{ Auth::user()->avatar }} avatar">
                                                            </div>
                                                        </div>
                                                        <div class="panel"> 
                                                            <div class="panel-heading">
                                                                <h4>{{ ucwords(Auth::user()->name) }}</h4>
                                                            
                                                        
                                                                <p>{{ Auth::user()->firstname }}</p>
                                                            
                                                    <div class="user-email text-muted">{{ ucwords(Auth::user()->email) }}</div>
                                                    <br>
                                                    <p>{!! Auth::user()->bio !!}</p>
                                                    <a href="http://127.0.0.1:8000/dashboard/account/{{Auth::user()->id }}/edit" class="btn btn-primary">{{ __('voyager.profile.edit') }}</a>
                                                    <br>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
                

    @endsection
@else
        <script>window.location.href = "/login";</script>   
@endif