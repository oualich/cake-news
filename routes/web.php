<?php
use Illuminate\Support\Facades\Storage;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


        Route::group(['prefix' => 'admin'], function () {
            Voyager::routes();
        });
      /**
         * Pages route 
         */

    // about routes...
    Route::get('/about', 'PagesController@getAbout'); 
    
    // communautes routes..
    Route::get('/communautes', 'PagesController@getCommunautes'); 

    //  contact routes..
    Route::get('/contact', 'PagesController@getContact'); 
    
    //  categories routes..
    Route::get('/news', 'PagesController@getnews'); 
    Route::get('/science', 'PagesController@getscience'); 
    Route::get('/high-tech', 'PagesController@gethightech'); 
    Route::get('/politique', 'PagesController@getpolitique'); 
    Route::get('/search', 'PagesController@getSearch'); 



    /**
    * session routes
    */
    Auth::routes();

    // Route::get('/dashboard', 'PagesController@getDashboard',['middleware' => 'auth', function()
    // {
    //     //
    //     Route::get('/dashboard', 'PagesController@getDashboard'); 

    // }]);
    // Home routes...
    Route::get('/home', 'PagesController@getHome')->name('home'); 
    Route::get('/', 'PagesController@getHome')->name('home'); 
    // dashboard
    Route::get('/dashboard', 'PagesController@getDashboard')->name('dashboard'); 
    // list member dashboard
    Route::get('/dashboard/member', 'PagesController@getDashboardmembres')->name('dashboard.member'); 
    // post dashboard routes...
    Route::get('/dashboard/posts', 'PagesController@getDashboardpost')->name('dashboard.posts'); 
    Route::get('/dashboard/posts/create', 'PostsController@create')->name('dashboard.posts.create'); 
    
    // account dashboard routes..
    Route::resource('dashboard/account','UserController');
    Route::get('dashboard/account/edit/','UserController@edit');
    // post routes...
    Route::resource('post','PostsController');

    // post slug routes
    Route::get('/poste/{slug}-{id}/', 'PostsController@show');
    // Route::resource('poste','PostsController');

Route::group(['middleware' => ['web']], function (){
    
  

        // Route::group(array('before' => 'auth'), function()
    // {
        // post routes...
        // Route::resource('profile','UserController');
        // Route::get('profile/edit','UserController@edit');
        // post routes...
        // Route::resource('post','PostsController');
        // Route::get('/post/{$id}/edit','PostsController@edit');
    // });
});





